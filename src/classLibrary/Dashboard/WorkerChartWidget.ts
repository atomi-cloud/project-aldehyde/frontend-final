import {WorkerGraphOption, WorkerGraphOptionBase} from "src/classLibrary/Charting/WorkerChart";
import {GraphSetting} from "src/classLibrary/Charting/GraphSetting";
import {Api, ApiError} from "src/classLibrary/Core/Api";
import {Auth0} from "src/classLibrary/Core/Auth0";
import {FloorPrincipalResponse} from "src/classLibrary/APIModels/core/floorPrincipalResponse";
import {Err, Ok, Result} from "@hqoss/monads";
import {FloorResponse} from "src/classLibrary/APIModels/core/floorResponse";
import {ToStartEnd} from "src/classLibrary/Dashboard/DashboardUtility";
import {RelativeTimeConfig} from "src/classLibrary/Dashboard/Dashboard";


interface WorkerChartWidget {
    options: WorkerGraphOptionBase;
    setting: GraphSetting;
    floorId?: string;
    time: RelativeTimeConfig;
}

interface WorkerAnalyticsConstructor {
    option: WorkerGraphOption;
    settings: GraphSetting;
    days: string[];
    floor: FloorPrincipalResponse | null;
}

class WorkerChartParser {
    private readonly core: Api;
    private readonly analytics: Api;
    private readonly auth: Auth0;
    private readonly projectId: string;

    constructor(core: Api, analytics: Api, auth: Auth0, projectId: string) {
        this.core = core;
        this.analytics = analytics;
        this.auth = auth;
        this.projectId = projectId;
    }


    async LoadSettings(s: WorkerChartWidget, compareTime: Date): Promise<Result<WorkerAnalyticsConstructor, ApiError>> {
        const analyticsToken = await this.auth.AnalyticsToken;
        const daysResult = await this.analytics.Get<string[]>(`Analytics/${this.projectId}/Worker/${s.options.workerId}/days`, null, analyticsToken);
        if (daysResult.isErr()) {
            return Err(daysResult.unwrapErr());
        }
        const days = daysResult.unwrap();

        const [startTime, endTime] = ToStartEnd(s.time, compareTime);
        const option: WorkerGraphOption = {
            ...s.options,
            startTime,
            endTime,
        };


        if (s.floorId) {
            const coreToken = await this.auth.CoreToken;
            return (await this.core.Get<FloorResponse>(`Floor/${s.floorId}`, null, coreToken))
                .map(floorFull => {
                    const floor = floorFull.principal;
                    return {
                        floor,
                        days,
                        option,
                        settings: s.setting,
                    }
                });
        } else {
            return Ok({
                days,
                option,
                settings: s.setting,
                floor: null,
            })
        }
    }

}

export {WorkerChartParser, WorkerAnalyticsConstructor, WorkerChartWidget}
