import {
    AnalyticUnit,
    HourAnalytics,
    ICountAnalytics,
    ISecondsAnalytics,
    SegmentAggregateAnalytics,
    SegmentAggregateAnalyticsIntermediate
} from "src/classLibrary/APIModels/analytics/CommonAnalyticModels";
import {format} from "date-fns";
import {FloorChartValueType, GraphScope} from "src/classLibrary/Charting/FloorChart";
import {MergeUnit, MergeUnitAverage} from "src/classLibrary/Charting/reducer/IntermediateMerge";
import {DeepClone} from "src/classLibrary/Utility";
import {ConvertToIntermediate, ConvertToPreIntermediate} from "src/classLibrary/Charting/reducer/Averager";
import {
    ComputeSegmentAggregateIntermediateAverage,
    ComputeSegmentAggregatePreIntermediateAverage
} from "src/classLibrary/Charting/reducer/SegmentAverager";

function ScopeGeneric(analytics: SegmentAggregateAnalytics, f: (originalKey: string, value: ICountAnalytics | ISecondsAnalytics) => void) {
    for (let k in analytics.data) {
        if (analytics.data.hasOwnProperty(k)) {
            if (k.Take(3) === "or:") {
                f(k, analytics.data[k]);
            }
        }
    }
}

function ScopeToDayAverage(analytics: SegmentAggregateAnalytics) {
    const stepUp = analytics as any as SegmentAggregateAnalyticsIntermediate
    ScopeGeneric(analytics, (k, value) => {
        const date = new Date(k.Skip(3));
        const dateFormat = format(date, "yyyy/MM/dd");
        if (analytics.data[dateFormat] == null) {
            // Set up
            const copy = DeepClone(value);
            stepUp.data[dateFormat] = ConvertToIntermediate(ConvertToPreIntermediate(copy));
        } else {
            MergeUnitAverage(stepUp.data[dateFormat], value)
        }
        delete analytics.data[k]
    })
    const stepPreIntermediate = ComputeSegmentAggregateIntermediateAverage(stepUp);
    ComputeSegmentAggregatePreIntermediateAverage(stepPreIntermediate);
}

function ScopeToMonthAverage(analytics: SegmentAggregateAnalytics) {

    const stepUp = analytics as any as SegmentAggregateAnalyticsIntermediate
    ScopeGeneric(analytics, (k, value) => {
        const date = new Date(k.Skip(3));
        const dateFormat = format(date, "MMM yyyy");
        if (analytics.data[dateFormat] == null) {
            // Set up
            const copy = DeepClone(value);
            stepUp.data[dateFormat] = ConvertToIntermediate(ConvertToPreIntermediate(copy));
        } else {
            MergeUnitAverage(stepUp.data[dateFormat], value)
        }
        delete analytics.data[k]
    })
    const stepPreIntermediate = ComputeSegmentAggregateIntermediateAverage(stepUp);
    ComputeSegmentAggregatePreIntermediateAverage(stepPreIntermediate);
}

function ScopeToDay(analytics: SegmentAggregateAnalytics) {
    ScopeGeneric(analytics, (k, value) => {
        const date = new Date(k.Skip(3));
        const dateFormat = format(date, "yyyy/MM/dd");
        if (analytics.data[dateFormat] == null)
            analytics.data[dateFormat] = DeepClone(value);
        else {
            const a = analytics.data[dateFormat];
            MergeUnit(a, value)
        }
        delete analytics.data[k]
    })
}

function ScopeToMonth(analytics: HourAnalytics) {
    ScopeGeneric(analytics, (k, value) => {
        const date = new Date(k.Skip(3));
        const dateFormat = format(date, "MMM yyyy");
        if (analytics.data[dateFormat] == null)
            analytics.data[dateFormat] = DeepClone(value);
        else {
            MergeUnit(analytics.data[dateFormat], value as AnalyticUnit);
        }
        delete analytics.data[k]
    })
}

function ScopeToHour(analytics: HourAnalytics) {
    ScopeGeneric(analytics, (k, value) => {
        const date = new Date(k.Skip(3));
        const dateFormat = format(date, "dd MMM hh aa");
        if (analytics.data[dateFormat] == null)
            analytics.data[dateFormat] = DeepClone(value);
        else {
            MergeUnit(analytics.data[dateFormat], value as AnalyticUnit);
        }
        delete analytics.data[k]
    })
}

function ScopeHourlyAnalytics(analytics: HourAnalytics, scope: GraphScope, value: FloorChartValueType): HourAnalytics {
    return GraphScope.match(scope, {
        Day() {
            if (value == 'time') {
                ScopeToDay(analytics);
            } else {
                ScopeToDayAverage(analytics);
            }
            return analytics;
        }, Hour() {
            ScopeToHour(analytics);
            return analytics;
        }, Month() {
            if (value == 'time') {
                ScopeToMonth(analytics);
            } else {
                ScopeToMonthAverage(analytics);

            }
            return analytics;
        }
    })
}

export {ScopeHourlyAnalytics}
