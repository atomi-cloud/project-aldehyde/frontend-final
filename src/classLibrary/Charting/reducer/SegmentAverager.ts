import {
    SegmentAggregateAnalytics,
    SegmentAggregateAnalyticsIntermediate,
    SegmentAggregateAnalyticsPreIntermediate
} from "src/classLibrary/APIModels/analytics/CommonAnalyticModels";
import {
    ComputeIntermediateAverage,
    ComputePreIntermediateAverage,
    ConvertToIntermediate,
    ConvertToPreIntermediate
} from "src/classLibrary/Charting/reducer/Averager";

function ComputeSegmentAggregatePreIntermediateAverage(next: SegmentAggregateAnalyticsPreIntermediate): SegmentAggregateAnalytics {
    const ret = next as any as SegmentAggregateAnalytics;
    for (let k in next.data) {
        if (next.data.hasOwnProperty(k)) {
            ret.data[k] = ComputePreIntermediateAverage(next.data[k])
        }
    }
    return ret;
}

function ComputeSegmentAggregateIntermediateAverage(next: SegmentAggregateAnalyticsIntermediate): SegmentAggregateAnalyticsPreIntermediate {
    const ret = next as any as SegmentAggregateAnalyticsPreIntermediate;
    for (let k in next.data) {
        if (next.data.hasOwnProperty(k)) {
            ret.data[k] = ComputeIntermediateAverage(next.data[k])
        }
    }
    return ret;
}

function ConvertSegmentAggregateToIntermediate(next: SegmentAggregateAnalyticsPreIntermediate): SegmentAggregateAnalyticsIntermediate {
    const ret = next as any as SegmentAggregateAnalyticsIntermediate;
    for (let k in next.data) {
        if (next.data.hasOwnProperty(k)) {
            ret.data[k] = ConvertToIntermediate(next.data[k])
        }
    }
    return ret;
}

function ConvertSegmentAggregateToPreIntermediate(next: SegmentAggregateAnalytics): SegmentAggregateAnalyticsPreIntermediate {
    const ret = next as any as SegmentAggregateAnalyticsPreIntermediate;
    for (let k in next.data) {
        if (next.data.hasOwnProperty(k)) {
            ret.data[k] = ConvertToPreIntermediate(next.data[k])
        }
    }
    return ret;
}

export {
    ConvertSegmentAggregateToIntermediate,
    ConvertSegmentAggregateToPreIntermediate,
    ComputeSegmentAggregateIntermediateAverage,
    ComputeSegmentAggregatePreIntermediateAverage
}
