import {Widget} from "src/classLibrary/Dashboard/Dashboard";
import {Err, Ok, Option, Result, Some} from "@hqoss/monads";
import {Api, ApiError} from "src/classLibrary/Core/Api";
import {Auth0} from "src/classLibrary/Core/Auth0";
import {ProjectResponse} from "src/classLibrary/APIModels/core/projectResponse";
import {LoadImageAsBase64} from "src/classLibrary/Utility";

interface Layout {
    x: number;
    y: number;
    w: number;
    h: number;
    i: string;
}

interface IWidgetStore {

    LoadWidgets(): Promise<Result<{ [s: string]: Widget }, ApiError>>;

    AddWidget(w: Widget): Promise<Option<ApiError>>;

    LoadLayout(): Promise<Result<Layout[], ApiError>>;


    UpdateLayout(w: Layout[]): Promise<Option<ApiError>>;

}

class ServerWidgetStore implements IWidgetStore {


    private readonly core: Api;
    private readonly auth: Auth0;
    private readonly project: string;

    constructor(core: Api, auth: Auth0, project: string) {
        this.core = core;
        this.auth = auth;
        this.project = project;
    }

    async AddWidget(w: Widget): Promise<Option<ApiError>> {
        const token = await this.auth.CoreToken;
        const project = await this.core.Get<ProjectResponse>(`Project/${this.project}`, null, token);
        if (project.isErr()) return Some(project.unwrapErr());

        const p = project.unwrap();


        const dashboardReq = p.principal.fields.dashboard ?? [];
        if (dashboardReq[0] == null) {
            dashboardReq.push({
                dashboardData: {},
                dashboard: [],
            })
        }
        if (dashboardReq[0].dashboard == null) dashboardReq[0].dashboard = []
        dashboardReq[0].dashboardData[w.id] = w;
        const image = await LoadImageAsBase64(p.principal.logo);


        return await this.core.QPut(`Project/${this.project}`,
            {
                name: p.principal.name,
                description: p.principal.description,
                image,
                dashboard: dashboardReq,
            },
            token
        );
    }

    async LoadLayout(): Promise<Result<Layout[], ApiError>> {
        const token = await this.auth.CoreToken;
        const project = await this.core.Get<ProjectResponse>(`Project/${this.project}`, null, token);
        if (project.isErr()) return Err(project.unwrapErr());
        const p = project.unwrap();

        if (p.principal.fields.dashboard != null) {
            if (p.principal.fields.dashboard[0] != null) {
                return Ok(p.principal.fields.dashboard[0].dashboard ?? []);
            }
        }
        return Ok([]);
    }

    async LoadWidgets(): Promise<Result<{ [p: string]: Widget }, ApiError>> {
        const token = await this.auth.CoreToken;
        const project = await this.core.Get<ProjectResponse>(`Project/${this.project}`, null, token);
        if (project.isErr()) return Err(project.unwrapErr());
        const p = project.unwrap();

        if (p.principal.fields.dashboard != null) {
            if (p.principal.fields.dashboard[0] != null) {
                return Ok(p.principal.fields.dashboard[0].dashboardData ?? {});
            }
        }
        return Ok({});
    }

    async UpdateLayout(w: Layout[]): Promise<Option<ApiError>> {
        const token = await this.auth.CoreToken;
        const project = await this.core.Get<ProjectResponse>(`Project/${this.project}`, null, token);
        if (project.isErr()) return Some(project.unwrapErr());

        const p = project.unwrap();


        const dashboardReq = p.principal.fields.dashboard ?? [];
        if (dashboardReq[0] == null) {
            dashboardReq.push({
                dashboardData: {},
                dashboard: [],
            })
        }

        if (dashboardReq[0].dashboardData == null) dashboardReq[0].dashboardData = {}

        dashboardReq[0].dashboard = w;

        console.log(
            'Update Layout!!',
            {
                name: p.principal.name,
                description: p.principal.description,
                dashboard: dashboardReq,
            })

        const image = await LoadImageAsBase64(p.principal.logo);
        return await this.core.QPut(`Project/${this.project}`,
            {
                name: p.principal.name,
                description: p.principal.description,
                image,
                dashboard: dashboardReq,
            },
            token
        );
    }


}

// class WidgetStore implements IWidgetStore {
//     constructor(projectId: string) {
//         this.projectId = projectId;
//     }
//
//     private readonly projectId: string;
//
//     get WidgetKey() {
//         return `${this.projectId}-dashboard`;
//     }
//
//
//     get DashboardKey() {
//         return `${this.projectId}-dashboard-layout`;
//     }
//
//     LoadWidgets(): Promise<Result<{ [s: string]: Widget }, ApiError>> {
//         const r = localStorage.getItem(this.WidgetKey);
//         if (r == null) return WrapPromise(Ok({}));
//         return WrapPromise(Ok(JSON.parse(r)));
//     }
//
//     async AddWidget(w: Widget): Promise<Option<ApiError>> {
//         const current = await this.LoadWidgets();
//         if (current.isErr()) return WrapPromise(Some(current.unwrapErr()))
//         const c = current.unwrap();
//         c[w.id] = w;
//         const serial = JSON.stringify(c);
//         localStorage.setItem(this.WidgetKey, serial);
//         return WrapPromise(None);
//     }
//
//     LoadLayout(): Promise<Result<Layout[], ApiError>> {
//         const r = localStorage.getItem(this.DashboardKey);
//         if (r == null) return WrapPromise(Ok([]));
//         return WrapPromise(Ok(JSON.parse(r)));
//     }
//
//     UpdateLayout(w: Layout[]): Promise<Option<ApiError>> {
//         const serial = JSON.stringify(w);
//         localStorage.setItem(this.DashboardKey, serial);
//         return WrapPromise(None);
//     }
//
//
// }

export {Layout, IWidgetStore, ServerWidgetStore}
