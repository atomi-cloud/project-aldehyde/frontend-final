import {ChartData, ChartDataSets, ChartType} from "chart.js";
import tinycolor from "tinycolor2";
import iwanthue from "iwanthue";
import {ColorMode} from "src/classLibrary/Geometry/ColoredMapper";
import {of, Union} from "ts-union";
import {GraphScope} from "src/classLibrary/Charting/FloorChart";
import {ApplyAlpha} from "src/classLibrary/Utility";

const GraphValue = Union({
    Float: of<void>(),
    Precision: of<number>(),
    SignificantFigure: of<number>(),
    Int: of<void>(),
    Hours: of<void>(),
    Date: of<GraphScope>(),
    Original: of<void>(),
});

type GraphValue = typeof GraphValue.T;

const ChartDataSet = Union({
    Matrix: of<MatrixDataSet>(),
    Graph: of<GraphDataSet>(),
})
type ChartDataSet = typeof ChartDataSet.T;

type Graph = {
    type: ChartType,
    dependentType: GraphValue;
    independentType: GraphValue;
    valueType: GraphValue;
    data: ChartDataSet
}


type GraphDependent = {
    label: string,
    id: string;
    color: string,
    value: number,
}

type GraphIndependent = {
    label: string,
    id: string,
};

type MatrixDependent = {
    label: string | number,
    labelId: string;
    value: number,
    color: string;
}

type MatrixIndependent = {
    label: string | number,
}

type MatrixDataSet = Map<MatrixIndependent, MatrixDependent[]>

type GraphDataSet = Map<GraphIndependent, GraphDependent[]>

export const distinct = [
    '#DB6983',
    '#6CA2EA',
    '#EFCD5A',
    '#82BFC0',
    '#8D6CFD',
    '#CACBCF',
    '#DB6983',
    '#6CA2EA',
    '#EFCD5A',
    '#82BFC0',
    '#8D6CFD',
    '#CACBCF',
    '#DB6983',
    '#6CA2EA',
    '#EFCD5A',
    '#82BFC0',
    '#8D6CFD',
    '#CACBCF',
    '#DB6983',
    '#6CA2EA',
    '#EFCD5A',
    '#82BFC0',
    '#8D6CFD',
    '#CACBCF',
].reverse();

const colorblind = iwanthue(20, {
    colorSpace: [0, 360, 40, 70, 15, 85],
} as any);

function ChooseColor(color: string, setting: ColorMode, distinct: string[], colorblind: string[]) {
    switch (setting) {
        case "original":
            return color;
        case "distinct":
            return distinct.pop();
        case "colorblind":
            return colorblind.pop();
    }
}

function ToChartJsMatrix(data: MatrixDataSet, color: string): ChartData {

    let max = 0;
    let matrix: { data: any[], backgroundColor: Function } = {
        data: [],
        backgroundColor: () => {}
    }
    data.Each((k, v) => {
        v.Each(point => {
            const {label, value} = point;
            if (value > max) max = value;
            const element = {x: k.label, y: label, v: value};
            matrix.data.push(element)
        });
    })

    matrix.backgroundColor = function (ctx: any) {
        const value = ctx.dataset.data[ctx.dataIndex].v;
        const alpha = value / max;
        const {r, g, b} = tinycolor(color).toRgb();
        return `rgba(${r}, ${g}, ${b}, ${alpha})`;
    }

    return {
        datasets: [
            matrix as any
        ]
    };
}

function ToLineChart(data: MatrixDataSet, colorMode: ColorMode, alpha: number): ChartData {


    //            X Axis  => Technically Z Axis      Y Axis
    // dataset is time =>  (SubContractor      +      value)[]

    const d = JSON.parse(JSON.stringify(distinct));
    const cb = JSON.parse(JSON.stringify(colorblind));

    // time[] (x-axis)
    const keys = data.Keys();

    const labels = keys.Map(x => x.label);
    const index = keys.Map(({label}, i) => {return {label, i}})
                      .AsValue(({label}) => label);
    const dataSet: { [id: string]: ChartDataSets } = {};

    data.Each((k, val) => {
        val.Each(v => {
            if (dataSet[v.labelId] == null) {
                const color = ChooseColor(v.color, colorMode, d, cb);
                dataSet[v.labelId] = {
                    label: v.label as string,
                    data: labels.Map(_ => 0),
                    backgroundColor: ApplyAlpha(color!, alpha),
                    borderColor: color,
                }
            }
            dataSet[v.labelId].data![index.get(k.label)!.i] = v.value!

        })
    })
    const datasets = new Map<string, ChartDataSets>(Object.entries(dataSet))
        .Values();

    return {
        labels,
        datasets,
    };
}

function ToChartPie(data: GraphDataSet, colorMode: ColorMode, alpha: number): ChartData {
    const d = JSON.parse(JSON.stringify(distinct));
    const cb = JSON.parse(JSON.stringify(colorblind));

    const keys =
        data.Values()
            .Map(x => x.Map(({label, id, color}) => { return {label, id, color}}))
            .Flatten<{ label: string, id: string, color: string }>()
            .Unique(true);

    const labelsMap = keys
        .Each(e => {
            e.color = ChooseColor(e.color, colorMode, d, cb)!
        })
        .Map((e, i) => {return {e, i}})
        .AsValue(v => v.e.id)

    const labels = keys.Map(x => x.label);
    const dataSet: { [id: string]: ChartDataSets } = {};

    data.Each((k, v) => {

        v.Each(e => {

            if (dataSet[k.id] == null) {
                dataSet[k.id] = {
                    label: k.label,
                    data: labels.Map(_ => 0),
                    backgroundColor: labelsMap.Map((_, v) => ApplyAlpha(v.e.color, alpha)),
                    borderColor: labelsMap.Map((_, v) => v.e.color),
                }
            }
            dataSet[k.id]!.data![labelsMap.get(e.id)!.i] = e.value;
        })
    })
    const datasets = new Map<string, ChartDataSets>(Object.entries(dataSet))
        .Values();

    return {
        labels,
        datasets,
    }


}

function ToChartJs(data: GraphDataSet, colorMode: ColorMode, alpha: number): ChartData {

    const d = JSON.parse(JSON.stringify(distinct));
    const cb = JSON.parse(JSON.stringify(colorblind));
    const keys = data.Keys();
    const labels = keys.Map(x => x.label);
    const labelMap = keys.AsKey((_, i) => i)
                         .MapKey(k => k.id);
    const dataSet: { [id: string]: ChartDataSets } = {};

    data.Each((k, v) => {
        v.Each(e => {
            if (dataSet[e.id] == null) {
                let color = ChooseColor(e.color, colorMode, d, cb);
                const {r, g, b} = tinycolor(color).toRgb();
                const background = `rgba(${r},${g},${b}, ${alpha})`;
                dataSet[e.id] = {
                    label: e.label,
                    data: labels.Map(_ => 0),
                    backgroundColor: background,
                    borderColor: color,
                }
            }
            dataSet[e.id]!.data![labelMap.get(k.id)!] = e.value;
        })
    })
    const datasets = new Map<string, ChartDataSets>(Object.entries(dataSet))
        .Values();

    return {
        labels,
        datasets,
    }

}


export {
    ToChartJs,
    ToChartJsMatrix,
    GraphDependent,
    GraphIndependent,
    GraphDataSet,
    MatrixDataSet,
    MatrixDependent,
    MatrixIndependent,
    Graph,
    GraphValue,
    ChartDataSet,
    ToChartPie,
    ToLineChart
}
