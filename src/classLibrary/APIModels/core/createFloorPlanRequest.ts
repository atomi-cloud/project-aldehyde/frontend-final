/**
 * Core API v1 (OpenAPI: 3.0.1)
 *
 *
 * NOTE: This class is auto generated by openapi-typescript-client-api-generator.
 * Do not edit the file manually.
 */

import {FloorPlanRequest} from "./floorPlanRequest";
import {ZoneRequest} from "src/classLibrary/APIModels/core/zoneRequest";

export class CreateFloorPlanRequest {
    public mainRequest: FloorPlanRequest;

    public image: string;

    public name: string;

    public zoneInfo: { [s: string]: ZoneRequest };

    public height: number;

    public width: number;

    public fields: string;

    /**
     * Creates a CreateFloorPlanRequest.
     *
     * @param {FloorPlanRequest} mainRequest
     * @param {string} image
     * @param {string} name
     * @param {object} zoneInfo
     * @param {number} height
     * @param {number} width
     * @param {string} fields
     */
    constructor(mainRequest: FloorPlanRequest, image: string, name: string, zoneInfo: { [s: string]: ZoneRequest }, height: number, width: number, fields: string) {
        this.mainRequest = mainRequest;
        this.image = image;
        this.name = name;
        this.zoneInfo = zoneInfo;
        this.height = height;
        this.width = width;
        this.fields = fields;
    }
}
