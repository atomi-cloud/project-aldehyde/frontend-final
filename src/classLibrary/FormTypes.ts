interface FormElement {
    key: string;
    label: string;
    icon: string;
    tooltip: string;
    baseType: 'basic' | 'number' | 'select' | 'color';
}

export type FormInput = FormElement | BasicInput | ColorInput | NumberInput | SelectInput

interface BasicInput extends FormElement {
    prefix?: string;
    suffix?: string;
    autogrow?: boolean;
    max?: number;
    bgColor?: string;
    color?: string;

    rules: any;

    type: 'text' | 'password' | 'email' | 'search' | 'url' | 'time' | 'date'

}

interface ColorInput extends FormElement {
    prefix?: string;
    suffix?: string;
    rules?: any[];
}

interface NumberInput extends FormElement {
    color?: string;
    prefix?: string;
    suffix?: string;
    bgColor?: string;

    autogrow?: boolean;
    rules?: any;
}


interface SelectInput extends FormElement {
    clearable?: boolean;
    show?: string;
    hide?: string;
    bgColor?: string;
    color?: string;
    multiple?: boolean;
    advance?: { labelKey: string, valueKey: string }
    chip?: boolean;
    options: any;
    newValueMode?: string;
    iconKey?: string;
    disableKey?: string;
}

interface BasicFormModel {
    models: { [s: string]: any }
    valid: boolean;
}

export {FormElement, BasicInput, NumberInput, SelectInput, ColorInput, BasicFormModel}
