import {QUploaderBase} from "quasar";
import {Component, Prop} from "vue-property-decorator";
import {ToBase64} from "src/classLibrary/Utility";


// @ts-ignore
@Component
export default class Base64Uploader extends QUploaderBase {

    // @ts-ignore
    @Prop() onUpload!: (i: string[]) => Promise<void>

    uploading = false;
    busy = false;

    isUploading() {
        return this.uploading;
    }

    isBusy() {
        return this.busy;
    }

    async upload() {
        const p = this as any;
        if (p.canUpload === false) {
            return
        }
        try {
            this.busy = true;
            this.uploading = true;
            const files = p.files as File[];
            const base64s = await Promise.all(files.Map(e => ToBase64(e)));
            await this.onUpload(base64s);
            this.$emit('success')
        } catch {
            this.$emit('failure')
        } finally {

            this.busy = false;
            this.uploading = false;
        }

    }
}
