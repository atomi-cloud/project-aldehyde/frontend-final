import {MetricValue, SingleMetric, SingleMetricQuerier} from "src/classLibrary/Dashboard/SingeMetricQuery";
import {ApiError} from "src/classLibrary/Core/Api";
import {Err, Ok, Result} from "@hqoss/monads";

interface CompareWidgetConstructor {
    title: string;
    left: string;
    right: string;
    leftValue: MetricValue;
    rightValue: MetricValue;
}

interface CompareTextWidget {
    leftMetric: SingleMetric;
    rightMetric: SingleMetric;

    title: string;
    left: string;
    right: string;

}

class CompareWidgetParser {
    private metricClient: SingleMetricQuerier;

    constructor(metricClient: SingleMetricQuerier) {
        this.metricClient = metricClient;
    }

    async LoadSetting(settings: CompareTextWidget): Promise<Result<CompareWidgetConstructor, ApiError>> {
        const leftResult = await this.metricClient.Query(settings.leftMetric);
        const rightResult = await this.metricClient.Query(settings.rightMetric);
        if (leftResult.isErr()) return Err(leftResult.unwrapErr())
        if (rightResult.isErr()) return Err(rightResult.unwrapErr())
        const left = leftResult.unwrap(), right = rightResult.unwrap();
        const cstr: CompareWidgetConstructor = {
            title: settings.title,
            left: settings.left,
            right: settings.right,
            leftValue: left,
            rightValue: right
        };
        return Ok(cstr);
    }

}

export {CompareWidgetParser, CompareWidgetConstructor, CompareTextWidget}
