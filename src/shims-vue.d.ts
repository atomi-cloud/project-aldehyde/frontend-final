declare module '*.vue' {
    import Vue from 'vue';
    export default Vue;
}
declare module "*.jpg";
declare module "*.jpeg";
declare module "*.png";
declare module "*.gif";
declare module "ExtendableEvent";
declare module 'vueg';
declare module 'vue-croppa';
declare module 'vue-panzoom'
declare module 'valid-web-color';
declare module 'vue-grid-layout';
