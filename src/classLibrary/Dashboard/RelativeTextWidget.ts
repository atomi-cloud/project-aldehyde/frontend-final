import {MetricValue, SingleMetric, SingleMetricQuerier} from "src/classLibrary/Dashboard/SingeMetricQuery";
import {Err, Ok, Result} from "@hqoss/monads";
import {ApiError} from "src/classLibrary/Core/Api";

interface RelativeWidgetConstructor {
    value: MetricValue;
    prev: MetricValue;
    overline: string;
    unit: string;
}


interface RelativeTextWidget {
    value: SingleMetric;
    prev: SingleMetric;
    overline: string;
    unit: string;
}

class RelativeTextParser {
    private readonly metricClient: SingleMetricQuerier


    constructor(metricClient: SingleMetricQuerier) {
        this.metricClient = metricClient;
    }

    async LoadSettings(r: RelativeTextWidget): Promise<Result<RelativeWidgetConstructor, ApiError>> {
        const valueResult = await this.metricClient.Query(r.value);
        const prevResult = await this.metricClient.Query(r.prev);
        if (valueResult.isErr()) return Err(valueResult.unwrapErr())
        if (prevResult.isErr()) return Err(prevResult.unwrapErr())
        const value = valueResult.unwrap(), prev = prevResult.unwrap();
        return Ok(
            {
                overline: r.overline,
                unit: r.unit,
                value,
                prev,
            }
        )

    }
}

export {RelativeWidgetConstructor, RelativeTextWidget, RelativeTextParser}
