import {Api} from "src/classLibrary/Core/Api";
import {Notify} from "quasar";
import {WarnMessage} from "src/classLibrary/Utility";

export interface Streamer<T> {
    Start(s: (s: T) => void): void;

    Stop(): void;
}

type PollLink = string | (() => string);

export class Poller<T> implements Streamer<T> {


    pollSpeed: number;
    api: Api;
    tokenFunction: () => Promise<string>
    link: PollLink;


    started = false;
    timeOut?: any | null;
    onPoll: (s: T) => void = () => {};


    Start(s: (s: T) => void): void {
        this.onPoll = s;
        this.started = true;
        this.startPolling().then();
    }

    private async startPolling() {
        if (!this.started) return;
        const token = await this.tokenFunction();
        let link = '';
        if (typeof this.link === "function") {
            link = this.link();
        } else {
            link = this.link as string;
        }

        const r = await this.api.Get<T>(link, null, token);
        const p = this;
        r.match({
            ok: (t) => {
                p.onPoll(t);
            },
            err: (e) => {
                Notify.create(WarnMessage('failed to poll'));
                console.error(e)
            }
        })
        this.timeOut = setTimeout(() => {
            p.startPolling();
        }, this.pollSpeed);

    }

    Stop(): void {
        this.started = false;
        clearTimeout(this.timeOut)
    }

    constructor(pollSpeed: number, api: Api, tokenFunction: () => Promise<string>, link: PollLink) {
        this.pollSpeed = pollSpeed;
        this.api = api;
        this.tokenFunction = tokenFunction;
        this.link = link;
    }


}
