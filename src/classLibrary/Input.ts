interface Input {
    key: string;
    rules: any[];
    label: string;
    tooltip: string;
    icon: string;

    max?: number;
    autoGrow?: boolean;
    type?: string;
    defaultValue?: any;
}

export {Input}
