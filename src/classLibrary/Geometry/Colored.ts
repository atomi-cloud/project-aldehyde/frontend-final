import {Q, Quad, QuadMap} from "src/classLibrary/Geometry/Quad";
import {P, Point, PointMap} from "src/classLibrary/Geometry/Point";


interface ColoredFloorPlanData {
    width: number;
    height: number;
    quads: ColoredQuadData[];
    lines: ColoredLineData[];
}

interface ColoredQuadData {
    danger: boolean;
    quad: Quad;
    color: string;
    name: string
}

interface ColoredLineData {

    p1: Point;
    p2: Point;
    color1: string;
    color2: string;
    name: string;

}

class ColoredFloorPlan {
    constructor(i: ColoredFloorPlanData) {
        this.width = i.width;
        this.height = i.height;
        this.quads = i.quads.Map(x => new ColoredQuad(x));
        this.lines = i.lines.Map(x => new ColoredLine(x));
    }

    get Data(): ColoredFloorPlanData {
        return {
            width: this.width,
            height: this.height,
            quads: this.quads.Map(x => x.Data),
            lines: this.lines.Map(x => x.Data)
        }
    }

    width: number;
    height: number;
    quads: ColoredQuad[];
    lines: ColoredLine[];

}

class ColoredQuad {
    constructor(quad: ColoredQuadData) {
        this.danger = quad.danger;
        this.name = quad.name;
        this.color = quad.color;
        this.quad = Q(quad.quad);
    }

    get Data(): ColoredQuadData {
        return {
            quad: this.quad.Quad,
            color: this.color,
            danger: this.danger,
            name: this.name
        }
    }

    danger: boolean;
    quad: QuadMap;
    color: string;
    name: string

}

class ColoredLine {
    constructor(p1: ColoredLineData) {
        this.p1 = P(p1.p1);
        this.p2 = P(p1.p2);
        this.color1 = p1.color1;
        this.color2 = p1.color2;
        this.name = p1.name;
    }

    get Data(): ColoredLineData {
        return {
            color1: this.color1,
            color2: this.color2,
            name: this.name,
            p1: this.p1.Point,
            p2: this.p2.Point,
        }
    }

    p1: PointMap;
    p2: PointMap;
    color1: string;
    color2: string;
    name: string;

}


export {ColoredLine, ColoredQuad, ColoredFloorPlan, ColoredFloorPlanData, ColoredQuadData, ColoredLineData}
