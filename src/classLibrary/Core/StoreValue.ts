interface StoreValue {
    project: string
    projectName: string;

}

const StoreValueDefault: StoreValue = {
    project: '',
    projectName: '',
}

export {
    StoreValue,
    StoreValueDefault
}
