import createAuth0Client, {Auth0ClientOptions} from "@auth0/auth0-spa-js";
import {Auth0} from "src/classLibrary/Core/Auth0";
import VueRouter from 'vue-router';

export async function GenerateAuth0(router: VueRouter) {
    const domain = process.env.AUTH0_DOMAIN ?? "" as string;
    const clientId = process.env.AUTH0_CLIENT_ID ?? "" as string;
    const callback = (appState: any) => {
        router.push(
            appState && appState.targetUrl
                ? appState.targetUrl
                : window.location.pathname
        );
    }
    const options = {
        domain,
        client_id: clientId,
        redirect_uri: window.location.origin

    } as Auth0ClientOptions;
    const auth0client = await createAuth0Client(options);

    return new Auth0(auth0client, callback);
}
