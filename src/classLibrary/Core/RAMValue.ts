import {Tab} from "src/classLibrary/Core/StrongTyping/Tabs";

interface RAMValue {
    title: string;
    leftIcon: string;
    rightIcon: string;
    tabs: Tab[];
    timeZone: string[]
    loading: boolean;
    filter: string;


}


const RAMValueDefault: RAMValue = {
    tabs: [],
    title: 'Builderlytics',
    leftIcon: 'menu',
    rightIcon: '',
    loading: false,
    filter: '',
    timeZone: [],
}

export {
    RAMValue,
    RAMValueDefault
}
