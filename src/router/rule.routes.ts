import SimpleCreateRule from "components/Forms/Rule/SimpleCreateRule.vue";
import Rules from "pages/Rules.vue";
import {RouteConfig} from "vue-router";

export const ruleRoutes: RouteConfig[] = [
    {
        name: 'create-rule',
        path: '/rule/create/simple',
        component: SimpleCreateRule,
        meta: {
            limbo: false,
            auth: [false, true]
        }
    },
    {
        name: 'rules',
        path: '/rule',
        component: Rules,
        meta: {
            limbo: false,
            auth: [false, true]
        }
    },
]
