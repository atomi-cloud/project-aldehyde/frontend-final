import {
    AggregateProjectionData,
    AnalyticUnit,
    HourAnalytics,
    IProjectionData,
    IsTimeBased,
    ProjectionData
} from "src/classLibrary/APIModels/analytics/CommonAnalyticModels";


function SecondToHourUnit(unit: ProjectionData) {
    for (let inner in unit) {
        if (unit.hasOwnProperty(inner)) {
            for (let outer in unit[inner]) {
                if (unit[inner].hasOwnProperty(outer)) {
                    unit[inner][outer] = unit[inner][outer] / 3600;
                }
            }
        }
    }
}

function SecondToHourAggregate(analytics: AggregateProjectionData) {
    for (let k in analytics) {
        if (analytics.hasOwnProperty(k)) {
            analytics[k] = analytics[k] / 3600;
        }
    }
}

function SecondToHourAggregateSegments(analytics: HourAnalytics) {
    for (let k in analytics.data) {
        if (analytics.data.hasOwnProperty(k)) {
            const a = analytics.data[k] as AnalyticUnit;
            SecondToHour(a);
            if (IsTimeBased(a)) {
                SecondToHourAggregate(a.seconds);
            } else {
                SecondToHourAggregate(a.workers);
            }
        }
    }
}

function SecondToHour(analytics: IProjectionData) {
    for (let inner in analytics.data) {
        if (analytics.data.hasOwnProperty(inner)) {
            for (let outer in analytics.data[inner]) {
                if (analytics.data[inner].hasOwnProperty(outer)) {
                    analytics.data[inner][outer] = analytics.data[inner][outer] / 3600;
                }
            }
        }
    }
}

export {
    SecondToHourAggregate, SecondToHourAggregateSegments, SecondToHour,
    SecondToHourUnit
}
