import {ApiObjectError} from "src/classLibrary/Core/Api";
import Vue from 'vue';
import {StandardDotNetError} from "src/classLibrary/Core/Standard";
import {add, formatDuration, intervalToDuration} from "date-fns";
import tinycolor from "tinycolor2";

const $$ = (i: number): Promise<void> => {
    return new Promise<void>(r => setTimeout(r, i))
}

function DeepClone<T>(i: T): T {
    return JSON.parse(JSON.stringify(i));
}

function ApplyAlpha(color: string, alpha: number): string {
    const {r, g, b} = tinycolor(color).toRgb();
    return `rgba(${r}, ${g} ,${b}, ${alpha})`;
}

const durationTable: any = {
    years: 'y',
    months: 'm',
    weeks: 'w',
    days: 'd',
    hours: 'h',
    minutes: 'min',
    seconds: 's',
}

function FormatShortDuration(duration: Duration, figure: number): string {
    let chosen = 0;
    let d = ['years', 'months', 'weeks', 'days', 'hours', 'minutes', 'seconds']
    let r = '';
    for (let k in d) {
        if (d.hasOwnProperty(k)) {
            const v = d[k];
            const rd = (duration as any)[v]
            if (rd != null && rd != 0) {
                chosen++;
                r += `${(duration as any)[v]}${durationTable[v]} `;
                if (chosen === figure) break;
            }
        }
    }
    return r


}

function FormatDuration(duration: Duration, short: boolean): string {
    const start = new Date();
    const end = add(start, duration);
    const newDuration = intervalToDuration({
        start,
        end
    });
    if (short) {
        const a = FormatShortDuration(newDuration, 2)
        return a;
    }

    return formatDuration(newDuration);
}

export function ApplyDiffGeneric(target: any, newValue: any, method: Function) {
    const targetType = typeof target;
    const newValueType = typeof newValue;
    if (targetType !== "object" || newValueType !== targetType) {
        return false;
    }
    // For array
    if (Array.isArray(target)) {
        if (Array.isArray(newValue)) {
            const diff = (target.length - newValue.length).Abs();
            if (target.length > newValue.length) {
                for (let i = 0; i < diff; i++) target.pop();
            } else if (diff != 0) {
                for (let i = target.length; i < newValue.length; i++)
                    target.push(newValue[i]);
            }
            for (let i = 0; i < target.length; i++) {
                const s = ApplyDiff(target[i], newValue[i]);
                if (!s) method(target, i, newValue[i]);
            }
            return true;
        } else {
            return false;
        }
    } else if (target instanceof Date) {
        if (newValue instanceof Date) {
            target.setDate(newValue.getDate());
            return true;
        } else {
            return false;
        }
    } else if (target instanceof Function || target instanceof RegExp) {
        return false;
    } else if (target instanceof Object) {
        if (newValue instanceof Object) {
            // remove everything that doesn't exist
            for (let k in target) {
                if (target.hasOwnProperty(k)) {
                    const v = newValue[k];
                    if (v === undefined) {
                        delete target[k]
                    } else if (v == null) {
                        method(target, k, null);
                    }
                }
            }

            // Recursively Apply Diff
            for (let k in target) {
                if (target.hasOwnProperty(k)) {
                    const s = ApplyDiff(target[k], newValue[k]);
                    if (!s) method(target, k, newValue[k]);
                }
            }

            // Assign new value
            for (let k in newValue) {
                if (newValue.hasOwnProperty(k)) {
                    if (target[k] == null) {
                        method(target, k, newValue[k]);
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

export function VueApplyDiff(target: any, newValue: any): boolean {
    const targetType = typeof target;
    const newValueType = typeof newValue;
    if (targetType !== "object" || newValueType !== targetType) {
        return false;
    }


    // For array
    if (Array.isArray(target)) {
        if (Array.isArray(newValue)) {
            const diff = (target.length - newValue.length).Abs();
            if (target.length > newValue.length) {
                for (let i = 0; i < diff; i++) target.pop();
            } else if (diff != 0) {
                for (let i = target.length; i < newValue.length; i++)
                    target.push(newValue[i]);
            }
            for (let i = 0; i < target.length; i++) {
                const s = ApplyDiff(target[i], newValue[i]);
                if (!s) Vue.set(target, i, newValue[i]);
            }
            return true;
        } else {
            return false;
        }
    } else if (target instanceof Date) {
        if (newValue instanceof Date) {
            target.setDate(newValue.getDate());
            return true;
        } else {
            return false;
        }
    } else if (target instanceof Function || target instanceof RegExp) {
        return false;
    } else if (target instanceof Object) {
        if (newValue instanceof Object) {
            // remove everything that doesn't exist
            for (let k in target) {
                if (target.hasOwnProperty(k)) {
                    const v = newValue[k];
                    if (v === undefined) {
                        delete target[k]
                    } else if (v == null) {
                        Vue.set(target, k, null);
                    }
                }
            }

            // Recursively Apply Diff
            for (let k in target) {
                if (target.hasOwnProperty(k)) {
                    const s = ApplyDiff(target[k], newValue[k]);
                    if (!s) Vue.set(target, k, newValue[k]);
                }
            }

            // Assign new value
            for (let k in newValue) {
                if (newValue.hasOwnProperty(k)) {
                    if (target[k] == null) {
                        Vue.set(target, k, newValue[k]);
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

export function ApplyDiff(target: any, newValue: any): boolean {
    const targetType = typeof target;
    const newValueType = typeof newValue;
    if (targetType !== "object" || newValueType !== targetType) {
        return false;
    }


    // For array
    if (Array.isArray(target)) {
        if (Array.isArray(newValue)) {
            const diff = (target.length - newValue.length).Abs();
            if (target.length > newValue.length) {
                for (let i = 0; i < diff; i++) target.pop();
            } else if (diff != 0) {
                for (let i = target.length; i < newValue.length; i++)
                    target.push(newValue[i]);
            }
            for (let i = 0; i < target.length; i++) {
                const s = ApplyDiff(target[i], newValue[i]);
                if (!s) target[i] = newValue[i];
            }
            return true;
        } else {
            return false;
        }
    } else if (target instanceof Date) {
        if (newValue instanceof Date) {
            target.setDate(newValue.getDate());
            return true;
        } else {
            return false;
        }
    } else if (target instanceof Function || target instanceof RegExp) {
        return false;
    } else if (target instanceof Object) {
        if (newValue instanceof Object) {
            // remove everything that doesn't exist
            for (let k in target) {
                if (target.hasOwnProperty(k)) {
                    const v = newValue[k];
                    if (v === undefined) {
                        delete target[k]
                    } else if (v == null) {
                        target[k] = null;
                    }
                }
            }

            // Recursively Apply Diff
            for (let k in target) {
                if (target.hasOwnProperty(k)) {
                    const s = ApplyDiff(target[k], newValue[k]);
                    if (!s) target[k] = newValue[k];
                }
            }

            // Assign new value
            for (let k in newValue) {
                if (newValue.hasOwnProperty(k)) {
                    if (target[k] == null) {
                        target[k] = newValue[k]
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function WarnMessage(s: string) {
    return {
        icon: 'icofont-warning',
        color: 'negative',
        message: s,
    }
}

function SuccessMessage(s: string) {
    return {
        icon: 'check',
        color: 'positive',
        message: s,
    }
}

function ApplyValidation(errors: { [s: string]: string | null }, toggleErrors: { [s: string]: boolean }, error: ApiObjectError): string[] {
    const e = error.error as StandardDotNetError;
    const ret: string[] = [];

    for (let k in toggleErrors) {
        Vue.set(toggleErrors, k, false);
    }

    new Map<string, string[]>(Object.entries(e.errors))
        .Each((k, v) => {
                const key = k.toLowerCase();
                if (errors[key] != null) {
                    Vue.set(errors, key, v[0])
                    Vue.set(toggleErrors, key, true)

                } else {
                    ret.push(...v);
                }
            }
        );
    return ret;
}

async function ToBase64(file: File): Promise<string> {
    return new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = () => resolve(reader.result as string)
        reader.onerror = error => reject(error)
    })
}

function LoadImageAsBase64(image: string): Promise<string> {

    function convert(image: string, callback: (s: HTMLImageElement) => void) {
        const img = new Image();
        img.onload = function () {
            callback(img)
        }
        img.setAttribute('crossorigin', 'anonymous');
        img.src = image;
    }

    function GetBase64Image(img: string, callback: (s: string) => void) {
        convert(img, function (newImg) {
            const canvas = document.createElement("canvas");
            canvas.width = newImg.width;
            canvas.height = newImg.height;
            const ctx = canvas.getContext("2d")!;
            ctx.drawImage(newImg, 0, 0);
            const base64 = canvas.toDataURL("image/png");
            callback(base64)
        })
    }

    return new Promise<string>((r) => {
        GetBase64Image(image, r)
    });
}

function UrlB64ToUint8Array(base64String: string) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

function GetTimeZone(): string {
    return Intl.DateTimeFormat().resolvedOptions().timeZone;
}


const Orientation = {
    lock: function () {
        if ((ScreenOrientation as (any | null))?.lock) {
            (ScreenOrientation as (any | null)).lock("portrait");
        }
    },
    unlock: function () {
        if ((ScreenOrientation as (any | null))?.unlock) {
            (ScreenOrientation as (any | null))?.unlock();
        }
    }
}


export {
    Orientation,
    $$,
    ApplyValidation,
    ToBase64,
    LoadImageAsBase64,
    WarnMessage,
    SuccessMessage,
    UrlB64ToUint8Array,
    FormatDuration,
    ApplyAlpha,
    GetTimeZone,
    DeepClone,
}
