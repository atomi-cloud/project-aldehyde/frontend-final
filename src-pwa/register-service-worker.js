import {register} from 'register-service-worker'

register(process.env.SERVICE_WORKER_FILE, {

    ready(/* registration */) {
        console.info('Service worker is active.')
    },

    registered() {
        console.info('Service worker is active.')

    },

    cached(/* registration */) {
        // console.info('Content has been cached for offline use.')
    },

    updatefound(/* registration */) {
        // console.info('New content is downloading.')
    },

    updated(/* registration */) {
        // console.log('New content is available; please refresh.')
    },

    offline() {
        console.info('No internet connection found. App is running in offline mode.')
    },

    error(err) {
        console.error('Error during service worker registration:', err)
    }
})
