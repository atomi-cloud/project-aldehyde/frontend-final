import {date} from "quasar";
import {RelativeTimeConfig} from "src/classLibrary/Dashboard/Dashboard";
import subtractFromDate = date.subtractFromDate;

function ToStartEnd(rt: RelativeTimeConfig, compare: Date): [string, string] {
    const end = subtractFromDate(compare, {days: rt.start});
    const start = subtractFromDate(end, {days: rt.amount});
    return [start.toISOString(), end.toISOString()];
}

export {ToStartEnd}
