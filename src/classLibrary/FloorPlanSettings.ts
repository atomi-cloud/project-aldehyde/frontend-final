import {ColorMode} from "src/classLibrary/Geometry/ColoredMapper";
import {LabelShape, WorkerColorMode, WorkerLabelMode} from "src/classLibrary/Geometry/ShapeDrawer";

interface FloorPlanSetting {
    showZone: boolean;
    zoneAlpha: number;
    zoneColorMode: ColorMode;

    showDanger: boolean;
    dangerIntensity: number;

    zoneTextColor: string;
    zoneTextSize: number;
    showZoneText: boolean;

    drawExit: boolean;
    exitAlpha: number;
    exitThickness: number;

    showWorkers: boolean;
    workerDotSize: number;
    workerColorMode: WorkerColorMode;
    workerLabelShape: LabelShape;
    workerLabelMode: WorkerLabelMode;
    workerDisplayInactive: boolean;
    workerInactiveDuration: number;
    workerLabelSize: number;
    workerLabelColor: string;
    workerLabelBorderSize: number;
    workerLabelBorderColor: string;
    workerLabelBackgroundColor: string;
    workerLabelBackgroundTransparency: number;

    pulse: boolean;
}

const DefaultFloorPlanSetting: FloorPlanSetting = {
    zoneColorMode: 'original',
    showZone: true,
    showDanger: true,
    zoneAlpha: 0.2,
    dangerIntensity: 10,
    drawExit: true,
    exitAlpha: 1,
    exitThickness: 2,
    zoneTextColor: 'black',
    zoneTextSize: 20,
    showZoneText: true,
    showWorkers: true,
    workerLabelBorderColor: 'black',
    workerLabelBorderSize: 0,
    workerDotSize: 12,
    workerColorMode: 'subcon',
    workerLabelShape: 'droplet',
    workerDisplayInactive: false,
    workerInactiveDuration: 10,
    workerLabelMode: 'name-short',
    workerLabelSize: 4,
    workerLabelColor: 'black',
    workerLabelBackgroundColor: 'black',
    workerLabelBackgroundTransparency: 0,
    pulse: true,
}
export {DefaultFloorPlanSetting, FloorPlanSetting}
