import {Auth0} from "src/classLibrary/Core/Auth0";
import {Api, ApiError} from "src/classLibrary/Core/Api";
import {of, Union} from "ts-union";
import {ChartDataSet} from "src/classLibrary/Charting/DataSet";

import {Err, Ok, Result} from "@hqoss/monads";
import {DayAnalytics, HourAnalytics} from "src/classLibrary/APIModels/analytics/CommonAnalyticModels";
import {
    DayAnalyticsToChartData,
    DayAnalyticsToChartDataAll,
    HourAnalyticsToMatrixData
} from "src/classLibrary/Charting/reducer/Converter";
import {ReduceDayAnalytics, ReduceHourlyAnalytics} from "src/classLibrary/Charting/reducer/FloorReducer";
import {GenerateTimeArray} from "src/classLibrary/Charting/WorkerChart";


const GraphScope = Union({
    Hour: of<void>(),
    Day: of<void>(),
    Month: of<void>(),
});

type GraphScope = typeof GraphScope.T;

const FloorGraphType: { label: string, value: FloorGraphType, icon: string }[] = [
    {
        label: 'Bar',
        value: 'horizontalBar',
        icon: 'icofont-chart-bar-graph'
    },
    {
        label: 'Radar',
        value: 'radar',
        icon: 'icofont-chart-radar-graph'
    },
    {
        label: 'Pie',
        value: 'pie',
        icon: 'icofont-chart-pie'
    },
    {
        label: 'Doughnut',
        value: 'doughnut',
        icon: 'icofont-chart-pie-alt'
    },
    {
        label: 'Line',
        value: 'line',
        icon: 'icofont-chart-line'
    },
    {
        label: 'Cumulative',
        value: 'bar',
        icon: 'icofont-chart-growth'
    },
];

const GraphScopeType: { label: string, value: GraphScope }[] = [
    {
        label: 'Hour',
        value: GraphScope.Hour(),
    },
    {
        label: 'Day',
        value: GraphScope.Day(),
    },
    {
        label: 'Month',
        value: GraphScope.Month(),
    }
]

const FloorChartVariableType: { label: string, value: FloorChartVariableType }[] =
    [
        {
            label: 'SubContractor vs Zone',
            value: 'sub'
        },
        {
            label: 'Zone vs SubContractor',
            value: 'zone'
        }
    ]
const FloorChartValueType: { label: string, value: FloorChartValueType }[] = [
    {
        label: 'Number of Workers',
        value: 'value'
    },
    {
        label: 'Man Hours',
        value: 'time'
    },
]
type FloorChartVariableType = 'sub' | 'zone';
type FloorChartValueType = 'value' | 'time';
type FloorGraphType = 'horizontalBar' | 'radar' | 'doughnut' | 'polarArea' | 'pie' | 'line' | 'bar'

interface FloorGraphOptionBase {
    projectId: string;
    floorId: string;
    trades?: string[];
    merge: boolean;
    type: FloorGraphType;
    independent: FloorChartVariableType;
    valueType: FloorChartValueType;
    scope?: GraphScope;
}

interface FloorGraphOption extends FloorGraphOptionBase {
    startTime: string;
    endTime: string;
}


class FloorChartRetriever {
    private readonly auth: Auth0;
    private readonly core: Api;

    constructor(auth: Auth0, core: Api) {
        this.auth = auth;
        this.core = core;
    }

    async Query(opt: FloorGraphOption, days: string[], projectName: string): Promise<Result<ChartDataSet | null, ApiError[]>> {
        const valueType = opt.valueType === 'value' ? 'count' : 'seconds';
        const dependent = ((opt.independent === 'sub') !== opt.merge) ? 'subcontractor/zone' : 'zone/subcontractor';
        const query = opt.trades?.Map(x => `trades=${x}`)?.join("&");
        const scope = (opt.type === 'line' || opt.type === 'bar') ? 'hour' : 'day';
        const endpoints: string[] = GenerateTimeArray(opt.startTime, opt.endTime, days,
            d => `Analytics/${opt.projectId}/${opt.floorId}/${d}/${scope}/${dependent}/${valueType}?${query}`);
        const token = await this.auth.AnalyticsToken;
        if (opt.type !== 'line' && opt.type !== 'bar') {
            const analytics = await Promise.all(endpoints.Map(e => this.core.Get<DayAnalytics>(e, null, token)));
            if (analytics.Any(e => e.isErr())) {
                return Err(analytics.Where(e => e.isErr())
                                    .Map(e => e.unwrapErr()));
            }
            const result = analytics
                .Where((e: Result<DayAnalytics, ApiError>) => e.isOk())
                .Map(e => e.unwrap());
            if (result.length !== 0) {
                const r = ReduceDayAnalytics(result, opt.valueType);
                if (opt.merge) {
                    const graph = DayAnalyticsToChartDataAll(r, opt.independent, projectName);
                    return Ok(ChartDataSet.Graph(graph));
                } else {
                    const graph = DayAnalyticsToChartData(r, opt.independent);
                    return Ok(ChartDataSet.Graph(graph));
                }

            }
            return Ok(null);
        } else {
            const analytics = await Promise.all(endpoints.Map(e => this.core.Get<HourAnalytics>(e, null, token)));

            if (analytics.Any(e => e.isErr())) {
                return Err(analytics.Where(e => e.isErr())
                                    .Map(e => e.unwrapErr()));
            }
            const result = analytics
                .Where((e: Result<HourAnalytics, ApiError>) => e.isOk())
                .Map(e => e.unwrap());
            if (result.length !== 0) {
                const r = ReduceHourlyAnalytics(result, opt.valueType, opt.scope!);
                const matrix = HourAnalyticsToMatrixData(r, opt.independent)
                return Ok(ChartDataSet.Matrix(matrix));
            }
            return Ok(null);
        }

    }

}


export {
    GraphScope,
    FloorGraphType,
    GraphScopeType,
    FloorGraphOption,
    FloorChartRetriever,
    FloorChartValueType,
    FloorChartVariableType,
    FloorGraphOptionBase
}
