import {
    AggregateProjectionData,
    AnalyticUnit,
    DayAnalytics,
    HourAnalytics,
    IsTimeBased
} from "src/classLibrary/APIModels/analytics/CommonAnalyticModels";
import {
    distinct,
    GraphDataSet,
    GraphDependent,
    GraphIndependent,
    MatrixDataSet,
    MatrixDependent,
    MatrixIndependent
} from "src/classLibrary/Charting/DataSet";
import {SubContractorPrincipalResponse} from "src/classLibrary/APIModels/analytics/subContractorPrincipalResponse";
import {ZoneLightResponse} from "src/classLibrary/APIModels/analytics/zoneLightResponse";
import {WorkerTimeChartResponse} from "src/classLibrary/APIModels/analytics/workerTimeChartResponse";
import {FloorPrincipalResponse} from "src/classLibrary/APIModels/analytics/floorPrincipalResponse";
import {DeepClone} from "src/classLibrary/Utility";

//Worker
function WorkerAnalyticsToLineDataAll(analytics: Map<string, WorkerTimeChartResponse>): MatrixDataSet {
    const map = new Map<MatrixIndependent, MatrixDependent[]>();
    analytics.Each((timeKey, timeChart) => {
        const key = {label: timeKey}
        map.set(key, []);
        for (let floorId in timeChart.timeSpent) {
            if (timeChart.timeSpent.hasOwnProperty(floorId)) {
                const floorData = timeChart.timeSpent[floorId] as { [s: string]: number };
                const floor = timeChart.floors[floorId] as FloorPrincipalResponse;
                for (let zoneId in floorData) {
                    const zone = timeChart.zones[zoneId] as ZoneLightResponse;
                    const value = timeChart.timeSpent[floorId][zoneId];
                    map.get(key)?.push({
                        labelId: zone.id,
                        label: floor.name + " : " + zone.name,
                        value,
                        color: zone.color,
                    })
                }
            }
        }

    })
    return map;
}

function WorkerAnalyticsToLineData(analytics: Map<string, WorkerTimeChartResponse>, floor?: string): MatrixDataSet {
    const map = new Map<MatrixIndependent, MatrixDependent[]>();
    const d = DeepClone(distinct);
    analytics.Each((timeKey, timeChart) => {
        const key = {label: timeKey}
        map.set(key, []);
        if (floor) {
            for (let floorId in timeChart.timeSpent) {
                if (timeChart.timeSpent.hasOwnProperty(floorId) && floorId === floor) {
                    const floorData = timeChart.timeSpent[floorId] as { [s: string]: number };
                    for (let zoneId in floorData) {
                        const zone = timeChart.zones[zoneId] as ZoneLightResponse;
                        const value = timeChart.timeSpent[floorId][zoneId];
                        map.get(key)?.push({
                            labelId: zone.id,
                            label: zone.name,
                            value,
                            color: zone.color,
                        })
                    }
                }
            }
        } else {
            for (let floorId in timeChart.floorTimeSpent) {
                if (timeChart.floorTimeSpent.hasOwnProperty(floorId)) {
                    const value = timeChart.floorTimeSpent[floorId];
                    // console.log(DeepClone(timeChart), floorId, value)
                    const floorItem = timeChart.floors[floorId] as FloorPrincipalResponse
                    const color = d.pop()!;
                    map.get(key)?.push({
                        labelId: floorItem.id,
                        label: floorItem.name,
                        value,
                        color,
                    })
                }
            }
        }
    })
    return map;
}

function WorkerAnalyticsToChartData(analytics: WorkerTimeChartResponse, projectName: string): GraphDataSet {
    const floors = analytics.floors;
    const d = DeepClone(distinct);
    const map = new Map<GraphIndependent, GraphDependent[]>();
    const key = {
        label: projectName,
        id: projectName,
    }
    map.set(key, []);
    for (let floorId in analytics.timeSpent) {
        if (analytics.timeSpent.hasOwnProperty(floorId)) {
            const floor: FloorPrincipalResponse = floors[floorId];
            const floorData = analytics.timeSpent[floorId];
            const array = map.get(key)!;
            const color = d.pop()!;
            array.push({
                value: 0,
                color,
                id: floor.id,
                label: floor.name,
            })
            for (let zoneName in floorData) {
                if (floorData.hasOwnProperty(zoneName)) {
                    const value = analytics.timeSpent[floorId][zoneName];
                    const r = array.Find(e => e.id == floor.id);
                    if (r != null) {
                        r.value += value;
                    }
                }
            }
        }
    }
    return map;
}

function WorkerAnalyticsToZoneSpecificChartData(analytics: WorkerTimeChartResponse): GraphDataSet {
    const [zones, floors] = [analytics.zones, analytics.floors];
    const map = new Map<GraphIndependent, GraphDependent[]>();
    for (let floorId in analytics.timeSpent) {
        if (analytics.timeSpent.hasOwnProperty(floorId)) {
            const floor: FloorPrincipalResponse = floors[floorId];
            const key = {
                label: floor.name,
                id: floor.id,
            }
            map.set(key, []);
            const floorData = analytics.timeSpent[floorId];
            for (let zoneName in floorData) {
                if (floorData.hasOwnProperty(zoneName)) {
                    const value = analytics.timeSpent[floorId][zoneName];
                    const zone = zones[zoneName] as ZoneLightResponse;
                    map.get(key)?.push({
                        value,
                        color: zone.color,
                        id: zone.id,
                        label: floor.name + ":" + zone.name,
                    })
                }
            }
        }
    }
    return map;
}


// Floor
function HourAnalyticsToMatrixData(analytics: HourAnalytics, type: 'sub' | 'zone'): MatrixDataSet {
    const [zones, sub] = [analytics.zones, analytics.subcontractors];
    const map = new Map<MatrixIndependent, MatrixDependent[]>();
    for (let timeKey in analytics.data) {
        if (analytics.data.hasOwnProperty(timeKey)) {
            const key = {label: timeKey}
            map.set(key, []);
            const segment = analytics.data[timeKey] as AnalyticUnit;
            // switch base on segment
            let info: AggregateProjectionData;
            if (IsTimeBased(segment)) {
                info = segment.seconds;
            } else {
                info = segment.workers;
            }
            for (let subjectKey in info) {
                if (info.hasOwnProperty(subjectKey)) {
                    const value = info[subjectKey];
                    if (type === 'sub') {
                        const subject = sub[subjectKey] as SubContractorPrincipalResponse;
                        map.get(key)?.push({
                            label: subject.name,
                            labelId: subject.id,
                            value,
                            color: subject.color
                        })
                    } else {
                        const subject = zones[subjectKey] as ZoneLightResponse;
                        map.get(key)?.push({
                            label: subject.name,
                            labelId: subject.id,
                            value,
                            color: subject.color,
                        })
                    }
                }
            }
        }

    }
    return map;
}

function DayAnalyticsToChartDataAll(analytics: DayAnalytics, type: 'sub' | 'zone', projectName: string): GraphDataSet {
    const [zones, subs] = [analytics.zones, analytics.subcontractors];
    const map = new Map<GraphIndependent, GraphDependent[]>();
    const key = {
        label: projectName,
        id: projectName,
    };
    map.set(key, []);
    let info: AggregateProjectionData;
    if (IsTimeBased(analytics)) {
        info = analytics.seconds;
    } else {
        info = analytics.workers;
    }
    if (type === 'zone') {
        for (let subId in info) {
            if (info.hasOwnProperty(subId)) {
                const sub = subs[subId] as SubContractorPrincipalResponse;
                const value = info[subId];
                map.get(key)?.push({
                    id: sub.id,
                    color: sub.color,
                    value,
                    label: sub.name,
                })
            }
        }
    } else {
        for (let zoneId in info) {
            if (info.hasOwnProperty(zoneId)) {
                const zone = zones[zoneId] as ZoneLightResponse;
                const value = info[zoneId];
                map.get(key)?.push({
                    id: zone.id,
                    color: zone.color,
                    value,
                    label: zone.name,
                })
            }
        }
    }
    return map;
}


function DayAnalyticsToChartData(analytics: DayAnalytics, type: 'sub' | 'zone'): GraphDataSet {
    const [zones, sub] = [analytics.zones, analytics.subcontractors];
    const map = new Map<GraphIndependent, GraphDependent[]>();
    if (type === 'sub') {
        for (let subId in analytics.data) {
            if (analytics.data.hasOwnProperty(subId)) {
                const key = {
                    label: sub[subId].name,
                    id: subId
                };
                map.set(key, []);
                for (let zoneName in analytics.data[subId]) {
                    if (analytics.data[subId].hasOwnProperty(zoneName)) {
                        const value = analytics.data[subId][zoneName];
                        const zone = zones[zoneName] as ZoneLightResponse;
                        map.get(key)?.push({
                            value,
                            color: zone.color,
                            id: zone.id,
                            label: zoneName,
                        })
                    }
                }
            }
        }
    } else {
        for (let zoneName in analytics.data) {
            if (analytics.data.hasOwnProperty(zoneName)) {
                const zone = zones[zoneName] as ZoneLightResponse;
                const key = {
                    label: zoneName,
                    id: zone.id,
                };
                map.set(key, []);
                for (let subId in analytics.data[zoneName]) {
                    if (analytics.data[zoneName].hasOwnProperty(subId)) {
                        const value = analytics.data[zoneName][subId];
                        const s = sub[subId] as SubContractorPrincipalResponse;
                        map.get(key)?.push({
                            value,
                            color: s.color,
                            id: s.id,
                            label: s.name,
                        })
                    }
                }
            }
        }

    }
    return map;
}

export {
    DayAnalyticsToChartData,
    WorkerAnalyticsToChartData,
    HourAnalyticsToMatrixData,
    WorkerAnalyticsToZoneSpecificChartData,
    WorkerAnalyticsToLineData,
    WorkerAnalyticsToLineDataAll,
    DayAnalyticsToChartDataAll,
}
