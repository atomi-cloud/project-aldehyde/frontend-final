import {None, Option, Result} from "@hqoss/monads";
import {ApiError} from "src/classLibrary/Core/Api";

class Atomic<T> {

    private readonly up: () => Promise<Result<T, ApiError>>
    private readonly down: (r: T) => Promise<Option<ApiError>>

    private success: boolean | null = null;
    private result: T | null = null;

    async Do(): Promise<Result<T, ApiError>> {
        const r = await this.up();
        if (r.isOk()) {
            this.success = true;
            this.result = r.unwrap();
        } else {
            this.success = false;
        }
        return r;
    }

    async Undo(): Promise<Option<ApiError>> {
        if (this.success) {
            return await this.down(this.result!);
        } else {
            return None;
        }
    }

    constructor(up: () => Promise<Result<T, ApiError>>, down: (r: T) => Promise<Option<ApiError>>) {
        this.up = up;
        this.down = down;
    }


}

export {Atomic}
