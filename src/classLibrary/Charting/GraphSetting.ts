import {TitlePosition} from "src/classLibrary/Constants";

interface GraphSetting {
    lineAlpha: number;
    pieAlpha: number;
    radarAlpha: number;
    barAlpha: number;
    scatterAlpha: number;
    polarAlpha: number;
    colorMode: GraphColorMode;
    labelGraph: boolean;
    labelColor: string;
    labelBackgroundColor: string;
    labelBackgroundTransparency: number;
    labelSize: number;
    tension: number;
    stack: boolean;
    matrixColor: string;
    borderThickness: number;
    lineThickness: number;
    title: boolean;
    legend: boolean;
    titleFontSize: number;
    titleColor: string;
    titlePosition: TitlePosition;
    shortenDuration: boolean;
    zeroLabel: boolean;

}

type GraphColorMode = 'original' | 'distinct' | 'colorblind';

const DefaultGraphSetting: GraphSetting = {
    zeroLabel: false,
    title: true,
    legend: true,
    shortenDuration: true,
    lineAlpha: 0,
    barAlpha: 0.65,
    polarAlpha: 0.1,
    scatterAlpha: 0.2,
    pieAlpha: 0.75,
    stack: true,
    radarAlpha: 0.3,
    colorMode: 'original',
    labelGraph: true,
    labelColor: 'white',
    labelBackgroundColor: 'black',
    labelBackgroundTransparency: 0.3,
    labelSize: 12,
    tension: 0,
    matrixColor: 'blue',
    lineThickness: 5,
    borderThickness: 1,
    titleFontSize: 12,
    titleColor: 'black',
    titlePosition: 'top',
}

export {GraphColorMode, GraphSetting, DefaultGraphSetting};
