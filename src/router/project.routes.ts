import Projects from "pages/Projects.vue";
import CreateProject from "components/Forms/CreateProject.vue";
import Project from "pages/Entity/Project.vue";

export const projectRoutes = [
    {
        name: 'projects',
        path: '/project',
        component: Projects,
        meta: {
            projectless: true,
            limbo: false,
            auth: [false, true]
        }
    },
    {
        name: 'create-project',
        path: '/project/create',
        component: CreateProject,
        meta: {
            transition: {
                enter: 'touchPoint'
            },
            projectless: true,
            limbo: false,
            auth: [false, true]
        }
    },
    {
        name: 'project',
        path: '/project/home',
        component: Project,
        meta: {
            limbo: false,
            auth: [false, true]
        }
    }
]
