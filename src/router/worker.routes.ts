import Workers from "pages/Workers.vue";
import CreateWorker from "components/Forms/CreateWorker.vue";
import {Route, RouteConfig} from "vue-router";
import {Tab} from "src/classLibrary/Core/StrongTyping/Tabs";
import WorkerIndex from "pages/Worker/WorkerIndex.vue";
import WorkerAlerts from "pages/Worker/WorkerAlerts.vue";
import WorkerAnalytics from "pages/Worker/WorkerAnalytics.vue";


function WorkerTabs(route: Route): Tab[] {
    return [
        {
            label: 'OverView',
            link: `/worker/${route.params.subId}/${route.params.workerId}`,
            icon: 'track_changes'
        },
        {
            label: 'Analytics',
            link: `/worker/${route.params.subId}/${route.params.workerId}/analytics`,
            icon: 'icofont-chart-line',

        },
        {
            label: 'Alerts',
            link: `/worker/${route.params.subId}/${route.params.workerId}/alerts`,
            icon: 'icofont-notification'
        },
    ]
}

export const workerRoutes: RouteConfig[] = [
    {
        name: 'worker',
        path: '/worker/:subId/:workerId',
        component: WorkerIndex,
        meta: {
            limbo: false,
            auth: [false, true],
            tabs: WorkerTabs,
        }
    },
    {
        name: 'worker-analytics',
        path: '/worker/:subId/:workerId/analytics',
        component: WorkerAnalytics,
        meta: {
            limbo: false,
            auth: [false, true],
            tabs: WorkerTabs,
        }
    },
    {
        name: 'worker-alerts',
        path: '/worker/:subId/:workerId/alerts',
        component: WorkerAlerts,
        meta: {
            limbo: false,
            auth: [false, true],
            tabs: WorkerTabs,
        }
    },
    {
        name: 'workers',
        path: '/worker',
        component: Workers,
        meta: {
            limbo: false,
            auth: [false, true]
        }
    },

    {
        name: 'create-worker',
        path: '/worker/create',
        component: CreateWorker,
        meta: {
            limbo: false,
            auth: [false, true]
        }
    },
]
