import {boot} from "quasar/wrappers";
import {Kore} from "@kirinnee/core";
import RouteSyncPlugin from "src/plugins/RouteSyncPlugin";
import GStorePlugin from "src/plugins/GStorePlugin";
import vueg from 'vueg';
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/swiper-bundle.css'
import DarkPlugin from "src/plugins/DarkPlugin";
import ApiPlugin from "src/plugins/ApiPlugin";
import {Api} from "src/classLibrary/Core/Api";
import {SetupGStore} from "boot/modules/gstore";
import {GenerateAuth0} from "boot/modules/auth0";
import Auth0Plugin from "src/plugins/Auth0Plugin";
import {InternalAuth} from "src/classLibrary/Core/InternalAuth";
import InternalAuthPlugin from "src/plugins/InternalAuthPlugin";
import {Auth0} from "src/classLibrary/Core/Auth0";
import Croppa from 'vue-croppa';
import AsyncComputedPlugin from 'vue-async-computed'
import {RegisterServiceWorker} from "boot/modules/vapid";
import Vue from 'vue';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import Chart from "chart.js";

declare module 'vue/types/vue' {
    interface Vue {
        $pwaPrompt: Event
    }
}

window.addEventListener("beforeinstallprompt", event => {
    Vue.prototype.$pwaPrompt = event;
});

// core inits
export default boot(async ({Vue, app, router}) => {
    Chart.plugins.register(ChartDataLabels);

    // Basic Plugins
    Vue.use(RouteSyncPlugin);
    Vue.use(GStorePlugin);
    Vue.use(VueAwesomeSwiper)
    Vue.use(DarkPlugin);
    Vue.use(vueg, router);
    Vue.use(Croppa);
    Vue.use(AsyncComputedPlugin)

    // Core
    const core = new Kore();
    core.ExtendPrimitives();

    // Setup API
    console.log(process.env.CORE_ENDPOINT, process.env.ANALYTICS_ENDPOINT);
    const coreApi = new Api(process.env.CORE_ENDPOINT ?? "http://core.localhost:5000");
    const analyticsApi = new Api(process.env.ANALYTICS_ENDPOINT ?? "http://analytics.localhost:5000");
    Vue.use(ApiPlugin, {core: coreApi, analytics: analyticsApi});


    // Setup global store
    const [settings, ram] = SetupGStore(app);

    // Setup Auth
    const auth0 = await GenerateAuth0(router);
    Vue.use(Auth0Plugin, auth0);

    // (window as any).$auth = auth0;
    //
    // Internal Auth
    const internalAuth = new InternalAuth(coreApi, auth0, settings);
    Vue.use(InternalAuthPlugin, internalAuth);

    // Sync Login and Logout with Internal Auth
    await auth0.RegisterLogInCallback(async () => {
        await internalAuth.Registered();
    })
    await auth0.RegisterLogOutCallback(async () => {
        internalAuth.user = null;
        return new Promise<void>(r => r());
    })

    await auth0.Authenticate();

    if (process.env.MODE === "pwa")
        await RegisterServiceWorker(internalAuth);

    // Router
    router.beforeEach(async (to, from, next) => {

        const auth = Vue.prototype.$auth as Auth0;
        const internalAuth = Vue.prototype.$coreAuth;

        await auth.Authenticate();
        const loggedIn = auth.isAuthenticated;

        const completed = loggedIn && auth.user?.email_verified && internalAuth.user != null;

        if (to == from) return;
        if (!to.meta.auth && !to.meta.limbo) return next();

        const [pub, prv] = to.meta.auth;


        function SuccessRoute() {
            if (to.meta.tabs != null) {
                if (typeof to.meta.tabs === "function") {
                    ram.StoreValue.tabs = to.meta.tabs(to);
                } else {
                    ram.StoreValue.tabs = to.meta.tabs;
                }
            } else {
                ram.StoreValue.tabs = [];
            }
            return next();
        }

        // Special case
        if (to.meta.limbo && loggedIn && !completed) {
            return SuccessRoute();
        } else if (to.meta.limbo && loggedIn && completed) {

            return await router.push("/")
        }

        if (pub && prv) {

            return SuccessRoute();
        }
        if (pub && !prv) {
            if (!completed) {


                return SuccessRoute();
            }
        }
        if (!pub && prv) {
            if (completed) {
                return SuccessRoute();
            }
        }
        if (!loggedIn) {

            return await router.push("/home");

        }

        return await router.push('/register')
    })


});
