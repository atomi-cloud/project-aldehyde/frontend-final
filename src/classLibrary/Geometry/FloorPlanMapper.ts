import {Q} from "../Geometry/Quad";
import {P} from "../Geometry/Point";
import {FloorPlanFullResponse} from "src/classLibrary/APIModels/core/floorPlanFullResponse";
import {InputInfo, InputZone, ZoneMapper} from "src/classLibrary/Geometry/ZoneMapper";
import {ExitMapper, InputExit} from "src/classLibrary/Geometry/ExitMapper";
import {ZoneResponse} from "src/classLibrary/APIModels/core/zoneResponse";
import {CreateFloorPlanRequest} from "src/classLibrary/APIModels/core/createFloorPlanRequest";
import {AreaRequest} from "src/classLibrary/APIModels/core/areaRequest";
import {ZoneRequest} from "src/classLibrary/APIModels/core/zoneRequest";
import {ExitRequest} from "src/classLibrary/APIModels/core/exitRequest";
import {FloorPlanRequest} from "src/classLibrary/APIModels/core/floorPlanRequest";


function oMap<T>(o: { [s: string]: T }): Map<string, T> {
    return new Map(Object.entries(o));
}

const FloorPlanMapper = {

    From: function (r: FloorPlanFullResponse): [Map<string, InputZone>, Map<string, InputExit>, number, number] {

        const a = oMap<ZoneResponse>(r.info.zoneData);

        const zones = r.areas.Map(x => {
            const z = a.get(x.areaId)!;
            const i: InputZone = {
                color: z.color,
                danger: z.danger,
                maps: x.maps.Map(x => Q(x).FlattenedQuad),
                meta: z.data,
                tracked: x.isTracked,
            }
            return [i, z.name];
        })
            .AsValue(([, name]) => name as string)
            .MapValue(([z,]) => z as InputZone);

        const exits = r.exits.Map(x => {
            const a1 = a.get(x.area1)!.name;
            const a2 = a.get(x.area2)!.name;
            const e: InputExit = {
                a1,
                a2,
                p: [P(x.pt1).Point, P(x.pt2).Point]
            }
            return [e, x.exitId];
        }).AsValue(([, name]) => name as string)
            .MapValue(([z,]) => z as InputExit);


        return [zones, exits, r.info.width, r.info.height];
    },

    FromRequest: function (r: CreateFloorPlanRequest): [Map<string, InputZone>, Map<string, InputExit>, number, number] {

        const a = oMap<ZoneRequest>(r.zoneInfo);

        const zones = r.mainRequest.areas.Map(x => {
            const z = a.get(x.areaId)!;
            const i: InputZone = {
                color: z.color,
                danger: z.danger,
                maps: x.maps.Map(x => Q(x).FlattenedQuad),
                meta: z.data,
                tracked: x.isTracked,
            }
            return [i, x.areaId];
        })
            .AsValue(([, name]) => name as string)
            .MapValue(([z,]) => z as InputZone);

        const exits = r.mainRequest.exits.Map(x => {
            const e: InputExit = {
                a1: x.area1,
                a2: x.area2,
                p: [P(x.pt1).Point, P(x.pt2).Point]
            }
            return [e, x.exitId];
        }).AsValue(([, name]) => name as string)
            .MapValue(([z,]) => z as InputExit);
        return [zones, exits, r.width, r.height];
    },


    To: function (zones: { [s: string]: InputZone }, exits: { [s: string]: InputExit },
                  length: number,
                  width: number,
                  image: string,
                  name: string,
                  fields: string): CreateFloorPlanRequest {

        const zr = oMap(zones)
            .Map((k, v) => {
                const [a, d] = ZoneMapper.To(v, k);
                return [a, d];
            });
        const z = zr.Map(x => x[0] as AreaRequest);
        const d = zr
            .Map(([_, info]) => info as InputInfo);

        const zoneRequests: { [s: string]: ZoneRequest } = {};
        d.Each(z => {
            zoneRequests[z.name] = new ZoneRequest(z.danger, z.color, z.meta);
        });

        const e = oMap(exits).Map((k, v) => ExitMapper.To(v, k) as ExitRequest);
        const floorplan = new FloorPlanRequest(e, z);
        return new CreateFloorPlanRequest(
            floorplan,
            image,
            name,
            zoneRequests,
            length,
            width,
            fields
        );
    }

};

export {FloorPlanMapper};
