/**
 * Core API v1 (OpenAPI: 3.0.1)
 *
 *
 * NOTE: This class is auto generated by openapi-typescript-client-api-generator.
 * Do not edit the file manually.
 */

import {TagPrincipalResponse} from "./tagPrincipalResponse";
import {WorkerPrincipalResponse} from "./workerPrincipalResponse";

export class TagResponse {
    public tag: TagPrincipalResponse;

    public worker: WorkerPrincipalResponse;

    /**
     * Creates a TagResponse.
     *
     * @param {TagPrincipalResponse} tag
     * @param {WorkerPrincipalResponse} worker
     */
    constructor(tag: TagPrincipalResponse, worker: WorkerPrincipalResponse) {
        this.tag = tag;
        this.worker = worker;
    }
}
