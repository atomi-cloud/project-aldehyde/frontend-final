import {StoreValue, StoreValueDefault} from "src/classLibrary/Core/StoreValue";
import {RAMValue, RAMValueDefault} from "src/classLibrary/Core/RAMValue";
import Vue, {ComponentOptions} from 'vue';
import {DefaultFloorPlanSetting, FloorPlanSetting} from "src/classLibrary/FloorPlanSettings";
import {GStore} from "@kirinnee/gstore";
import {DefaultGraphSetting, GraphSetting} from "src/classLibrary/Charting/GraphSetting";

export function SetupGStore(app: ComponentOptions<Vue>): [
    {
        store: GStore<StoreValue>,
        floorplan: GStore<FloorPlanSetting>,
        graph: GStore<GraphSetting>,
    },
    GStore<RAMValue>
] {


    const ram = new GStore<RAMValue>(RAMValueDefault, false, "");
    const settings = {
        store: new GStore<StoreValue>(StoreValueDefault, true, "app-settings"),
        floorplan: new GStore<FloorPlanSetting>(DefaultFloorPlanSetting, true, "floorplan-setting"),
        graph: new GStore<GraphSetting>(DefaultGraphSetting, true, "graph-setting"),
    }
    const old = app.data;
    app.data = function () {
        let prev: any = {}
        if (typeof old == "function") {
            prev = (old as any)()
        } else if (typeof old === "object") {
            prev = old;
        }
        prev.persistentStore = settings.store;
        prev.floorPlanSetting = settings.floorplan;
        prev.graphSetting = settings.graph
        prev.store = ram;
        return prev;
    }
    return [settings, ram];
}
