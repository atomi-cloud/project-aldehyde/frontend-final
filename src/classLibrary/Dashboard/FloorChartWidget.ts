import {GraphSetting} from "src/classLibrary/Charting/GraphSetting";
import {Api, ApiError} from "src/classLibrary/Core/Api";
import {Auth0} from "src/classLibrary/Core/Auth0";
import {Result} from "@hqoss/monads";
import {FloorGraphOption, FloorGraphOptionBase} from "src/classLibrary/Charting/FloorChart";
import {ToStartEnd} from "src/classLibrary/Dashboard/DashboardUtility";
import {RelativeTimeConfig} from "src/classLibrary/Dashboard/Dashboard";

interface FloorChartWidget {
    options: FloorGraphOptionBase;
    setting: GraphSetting;
    time: RelativeTimeConfig;
}

interface FloorAnalyticsConstructor {
    options: FloorGraphOption;
    settings: GraphSetting;
    days: string[]
}

class FloorChartParser {
    private readonly analytics: Api;
    private readonly auth: Auth0;
    private readonly projectId: string;

    constructor(analytics: Api, auth: Auth0, projectId: string) {
        this.analytics = analytics;
        this.auth = auth;
        this.projectId = projectId;
    }

    async LoadSettings(s: FloorChartWidget, compareTime: Date): Promise<Result<FloorAnalyticsConstructor, ApiError>> {
        const [startTime, endTime] = ToStartEnd(s.time, compareTime);
        const options: FloorGraphOption = {
            ...s.options,
            startTime,
            endTime,
        };
        const token = await this.auth.AnalyticsToken;
        const daysResult = await this.analytics.Get<string[]>(`Analytics/${this.projectId}/${s.options.floorId}/days`, null, token);
        return daysResult.map(days => {
            return {
                days,
                options,
                settings: s.setting
            } as FloorAnalyticsConstructor
        });
    }

}

export {FloorChartParser, FloorAnalyticsConstructor, FloorChartWidget}
