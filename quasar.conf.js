const {configure} = require('quasar/wrappers');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = configure(function (ctx) {
    return {
        supportTS: true,
        boot: [
            'dependency_injection.ts',
            'assets.ts',
        ],
        css: [
            'app.scss'
        ],
        extras: [
            // 'ionicons-v4',
            // 'mdi-v5',
            // 'fontawesome-v5',
            // 'eva-icons',
            // 'themify',
            // 'line-awesome',
            // 'roboto-font-latin-ext', // this or either 'roboto-font', NEVER both!

            'roboto-font', // optional, you are not bound to it
            'material-icons', // optional, you are not bound to it
        ],
        build: {
            vueRouterMode: 'history', // available values: 'hash', 'history'
            env: {
                CORE_ENDPOINT: process.env.CORE_ENDPOINT,
                ANALYTICS_ENDPOINT: process.env.ANALYTICS_ENDPOINT,
                VAPID_KEY: process.env.VAPID_KEYS,
                AUTH0_DOMAIN: process.env.AUTH0_DOMAIN,
                AUTH0_CLIENT_ID: process.env.AUTH0_CLIENT_ID,
                AUTH0_CORE_AUD: process.env.AUTH0_CORE_AUD,
                AUTH0_ANALYTICS_AUD: process.env.AUTH0_ANALYTICS_AUD,
            },
            extendWebpack(cfg, {isServer, isClient}) {
                if (!ctx.dev) {
                    cfg.optimization.minimizer = [
                        new TerserPlugin({
                            terserOptions: {
                                compress: {
                                    drop_console: true,
                                    unsafe: false,
                                },
                                output: {comments: false},
                                toplevel: false
                            }
                        })
                    ]
                }
            },
            // transpile: false,

            // Add dependencies for transpiling with Babel (Array of string/regex)
            // (from node_modules, which are by default not transpiled).
            // Applies only if "transpile" is set to true.
            // transpileDependencies: [],

            // rtl: false, // https://quasar.dev/options/rtl-support
            // preloadChunks: true,
            // showProgress: false,
            // gzip: true,
            // analyze: true,

            // Options below are automatically set depending on the env, set them if you want to override
            // extractCSS: false,
        },

        devServer: {
            https: true,
            port: 8090,
            host: 'builderlytics'
        },

        // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-framework
        framework: {
            iconSet: 'material-icons', // Quasar icon set
            lang: 'en-us', // Quasar language pack
            config: {},
            importStrategy: 'auto',
            plugins: [
                'Notify'
            ]
        },
        // https://quasar.dev/options/animations
        animations: 'all',
        ssr: {
            pwa: true
        },
        pwa: {
            workboxPluginMode: 'InjectManifest', // 'GenerateSW' or 'InjectManifest'
            workboxOptions: {
                exclude: [/_redirects/]
            },
            manifest: {
                name: `Builderlytics`,
                short_name: `Builderlytics`,
                description: `WebApp to manage and monitor construction sites`,
                display: 'standalone',
                orientation: 'natural',
                // background_color: '#ffffff',
                theme_color: '#027be3',
                icons: [
                    {
                        src: 'icons/icon-128x128.png',
                        sizes: '128x128',
                        type: 'image/png'
                    },
                    {
                        src: 'icons/icon-192x192.png',
                        sizes: '192x192',
                        type: 'image/png'
                    },
                    {
                        src: 'icons/icon-256x256.png',
                        sizes: '256x256',
                        type: 'image/png'
                    },
                    {
                        src: 'icons/icon-384x384.png',
                        sizes: '384x384',
                        type: 'image/png'
                    },
                    {
                        src: 'icons/icon-512x512.png',
                        sizes: '512x512',
                        type: 'image/png'
                    }
                ]
            }
        },

        // Full list of options: https://quasar.dev/quasar-cli/developing-cordova-apps/configuring-cordova
        cordova: {
            // noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing
        },

        // Full list of options: https://quasar.dev/quasar-cli/developing-capacitor-apps/configuring-capacitor
        capacitor: {
            hideSplashscreen: true
        },

        // Full list of options: https://quasar.dev/quasar-cli/developing-electron-apps/configuring-electron
        electron: {
            bundler: 'packager', // 'packager' or 'builder'

            packager: {
                // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

                // OS X / Mac App Store
                // appBundleId: '',
                // appCategoryType: '',
                // osxSign: '',
                // protocol: 'myapp://path',

                // Windows only
                // win32metadata: { ... }
            },

            builder: {
                // https://www.electron.build/configuration/configuration

                appId: 'builderlytics'
            },

            // More info: https://quasar.dev/quasar-cli/developing-electron-apps/node-integration
            nodeIntegration: true,

            extendWebpack(/* cfg */) {
                // do something with Electron main process Webpack cfg
                // chainWebpack also available besides this extendWebpack
            }
        }
    }
});
