import {MetricValue, SingleMetric, SingleMetricQuerier} from "src/classLibrary/Dashboard/SingeMetricQuery";
import {ApiError} from "src/classLibrary/Core/Api";
import {Result} from "@hqoss/monads";

interface SingleValueWidgetConstructor {
    value: MetricValue;
    overline: string;
    unit: string;
}

interface SingleValueTextWidget {
    metricType: SingleMetric;
    overline: string;
    unit: string;
}

class SingleValueWidgetParser {

    private readonly metricClient: SingleMetricQuerier;


    async LoadSettings(widget: SingleValueTextWidget): Promise<Result<SingleValueWidgetConstructor, ApiError>> {
        const r = await this.metricClient.Query(widget.metricType);
        return r.map(value => {return {value, overline: widget.overline, unit: widget.unit}})
    }

    constructor(metricClient: SingleMetricQuerier) {
        this.metricClient = metricClient;
    }
}

export {SingleValueWidgetConstructor, SingleValueTextWidget, SingleValueWidgetParser}

