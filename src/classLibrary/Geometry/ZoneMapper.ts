import {AreaResponse} from "src/classLibrary/APIModels/core/areaResponse";
import {CoordinateMapResponse} from "src/classLibrary/APIModels/core/coordinateMapResponse";
import {CoordinateMapRequest} from "src/classLibrary/APIModels/core/coordinateMapRequest";
import {AreaRequest} from "src/classLibrary/APIModels/core/areaRequest";
import {FlattenedQuad, Q} from "src/classLibrary/Geometry/Quad";
import {ZoneResponse} from "src/classLibrary/APIModels/core/zoneResponse";


type Area = AreaRequest | AreaResponse;
type CoordMap = CoordinateMapResponse | CoordinateMapRequest;


interface InputZone {
    tracked: boolean;
    maps: FlattenedQuad[];
    danger: boolean;
    meta: { [s: string]: string[] };
    color: string;
}

interface InputInfo {
    danger: boolean;
    meta: { [s: string]: string[] };
    color: string;
    name: string;
}

const ZoneMapper = {

    From: function (zone: Area, zR: ZoneResponse): InputZone {

        const maps = (<CoordMap[]>zone.maps).Map(x => Q(x).FlattenedQuad);

        return {
            tracked: zone.isTracked,
            danger: zR.danger,
            meta: zR.data as { [s: string]: string[] },
            color: zR.color,
            maps,
        }
    },

    To: function (input: InputZone, name: string): [AreaRequest, InputInfo] {

        const out = input.maps.Map(x => Q(x).CoordinateMapRequest);

        return [
            new AreaRequest(name, input.tracked, out),
            {
                danger: input.danger,
                color: input.color,
                meta: input.meta,
                name,
            }
        ]
    }
};


export {InputZone, ZoneMapper, InputInfo}
