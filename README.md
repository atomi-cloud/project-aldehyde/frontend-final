# Builderlytics (builderlytics)
WebApp to manage and monitor construction sites

### Environment Variables

For local development, after setting up the Core API with the host file configured,
please set the following development environment values:


|KEY| VALUE|
|---|---|
| CORE_ENDPOINT | http://core.localhost:5000 |
| ANALYTICS_ENDPOINT | http://analytics.localhost:5000 |
| AUTH0_DOMAIN | local-builderlyitcs.au.auth0.com|
| AUTH0_ANALYTICS_AUD | http://analytics.localhost |
| AUTH0_CORE_AUD | http://core.localhost |
| AUTH0_CLIENT_ID | 8S4vSubN0tElnV8h7y1eZYe982HJk3Bf |



### Starting
run
```
yarn dev
```
Navigate to "https://builderlytics:8090"
