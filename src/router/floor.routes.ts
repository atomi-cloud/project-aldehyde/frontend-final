import CreateFloor from "components/Forms/CreateFloor.vue";
import CreateFloorPlan from "components/Forms/CreateFloorPlan.vue";
import {Route, RouteConfig} from 'vue-router';
import Floors from "pages/Floors.vue";
import FloorLiveView from "pages/Floors/FloorLiveView.vue";
import {Tab} from "src/classLibrary/Core/StrongTyping/Tabs";
import FloorPlanList from "pages/Floors/FloorPlanList.vue";
import AlertList from "pages/Floors/AlertList.vue";
import FloorRules from "pages/Floors/FloorRules.vue";
import FloorHistory from "pages/Floors/FloorHistory.vue";
import FloorAnalytics from "pages/Floors/FloorAnalytics.vue";
import FixedTimeFloorPlan from "pages/Floors/FixedTimeFloorPlan.vue";

function FloorTabs(route: Route): Tab[] {
    return [
        {
            label: 'Live View',
            link: `/floor/${route.params.floorId}`,
            icon: 'live_tv'
        },
        {
            label: 'FloorPlans',
            link: `/floor/${route.params.floorId}/floorplan`,
            icon: 'icofont-architecture',

        },
        {
            label: 'Alerts',
            link: `/floor/${route.params.floorId}/alert`,
            icon: 'icofont-notification'
        },
        {
            label: 'Analytics',
            link: `/floor/${route.params.floorId}/analytics`,
            icon: 'icofont-chart-line',

        },
        {
            label: 'History',
            link: `/floor/${route.params.floorId}/history`,
            icon: 'icofont-chart-flow'
        },
        {
            label: 'Rules',
            link: `/floor/${route.params.floorId}/rules`,
            icon: 'icofont-ruler-compass'
        }
    ]
}

export const floorRoutes: RouteConfig[] = [
    {
        name: 'floors',
        path: '/floor',
        component: Floors,
        meta: {
            limbo: false,
            auth: [false, true]
        }
    },
    {
        name: 'create-floor',
        path: '/floor/create',
        component: CreateFloor,
        meta: {
            limbo: false,
            auth: [false, true]
        }
    },

    {
        name: 'floor-floorplan',
        path: '/floor/:floorId/floorplan',
        component: FloorPlanList,
        meta: {
            limbo: false,
            auth: [false, true],
            tabs: FloorTabs,
        }
    },
    {
        name: 'floor-alert',
        path: '/floor/:floorId/alert',
        component: AlertList,
        meta: {
            limbo: false,
            auth: [false, true],
            tabs: FloorTabs,
        }
    },
    {
        name: 'floor-analytics',
        path: '/floor/:floorId/analytics',
        component: FloorAnalytics,
        meta: {
            limbo: false,
            auth: [false, true],
            tabs: FloorTabs,
        }
    },
    {
        name: 'floor-history',
        path: '/floor/:floorId/history',
        component: FloorHistory,
        meta: {
            limbo: false,
            auth: [false, true],
            tabs: FloorTabs,
        }
    },
    {
        name: 'floor-rules',
        path: '/floor/:floorId/rules',
        component: FloorRules,
        meta: {
            limbo: false,
            auth: [false, true],
            tabs: FloorTabs,
        }
    },
    {
        name: 'create-floorplan',
        path: '/floor/:floorId/floorplan/create',
        component: CreateFloorPlan,
        meta: {
            limbo: false,
            auth: [false, true]
        }
    },
    {
        name: 'live-view',
        path: '/floor/:floorId/',
        component: FloorLiveView,
        meta: {
            limbo: false,
            auth: [false, true],
            tabs: FloorTabs,
        },
        children: []
    },
    {
        name: 'floor-timetravel',
        path: '/floor/:floorId/timetravel',
        component: FixedTimeFloorPlan,
        meta: {
            limbo: false,
            auth: [false, true],
        },
        children: []
    },
];

