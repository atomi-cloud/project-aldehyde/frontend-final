import {PointFRequest} from "src/classLibrary/APIModels/core/pointFRequest";
import {PointFResponse} from "src/classLibrary/APIModels/core/pointFResponse";


type Point = [number, number];

function P(p: Point | PointFResponse | PointFRequest): PointMap {
    return new PointMap(p);
}

class PointMap {
    x: number;
    y: number;

    Angle(target: PointMap): number {
        const [x1, y1] = target.Point;
        const [x2, y2] = this.Point;

        if (x1 === y1 && y1 === x2 && x2 === y2 && y2 === 0) {
            return 0;
        }
        const xDiff = x1 - x2;
        const yDiff = y1 - y2;

        const x = xDiff > 0;
        const y = yDiff > 0;


        let degree = (Math.atan(yDiff / xDiff) * (180 / Math.PI)).Abs();
        if (x && y) {
            degree = 360 - degree
        } else if (!x && y) {
            degree = 180 + degree
        } else if (!x && !y) {
            degree = 180 - degree
        }


        return degree;
    }

    Distance(other: PointMap): number {
        const xDiff = (this.x - other.x).Abs();
        const yDiff = (this.y - other.y).Abs();
        return (xDiff ** 2 + yDiff ** 2).Root();
    }

    MidPoint(other: PointMap): PointMap {
        const [x1, y1] = this.Point;
        const [x2, y2] = other.Point;
        return new PointMap([(x1 + x2) / 2, (y1 + y2) / 2]);
    }

    Scale(x?: number, y?: number): PointMap {
        let X = this.x * (x ?? 1);
        let Y = this.y * (y ?? 1);
        return new PointMap([X, Y]);
    }

    Fraction(x?: number, y?: number): PointMap {

        let X = this.x / (x ?? 1);
        let Y = this.y / (y ?? 1);

        return new PointMap([X, Y]);
    }

    Percentage(x?: number, y?: number): PointMap {
        const [X, Y] = this.Fraction(x, y).Point;
        return new PointMap([X * 100, Y * 100]);
    }

    get Point(): Point {
        return [this.x, this.y];
    }

    get PointFResponse(): PointFResponse {
        return new PointFResponse(this.x, this.y);
    }

    get PointFRequest(): PointFRequest {
        return new PointFRequest(this.x, this.y);
    }

    constructor(p: Point | PointFResponse | PointFRequest) {


        if ((<Point>p)[0] !== undefined) {
            const [x, y] = (<Point>p);
            this.x = x;
            this.y = y;
            return;
        }

        if ((<PointFRequest | PointFResponse>p).x !== undefined) {
            const pt = (<PointFRequest | PointFResponse>p);
            this.x = pt.x;
            this.y = pt.y;
            return;
        }
        throw Error("unknown point type");
    }

}


export {P, Point, PointMap}
