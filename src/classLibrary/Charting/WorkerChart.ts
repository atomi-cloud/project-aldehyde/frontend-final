import {Auth0} from "src/classLibrary/Core/Auth0";
import {Api, ApiError} from "src/classLibrary/Core/Api";
import {Err, Ok, Result} from "@hqoss/monads";
import {ChartDataSet} from "src/classLibrary/Charting/DataSet";
import {add, format} from "date-fns";
import {WorkerTimeChartResponse} from "src/classLibrary/APIModels/analytics/workerTimeChartResponse";
import {ReduceWorkerTimeChart} from "src/classLibrary/Charting/reducer/WorkerReducer";
import {
    WorkerAnalyticsToChartData,
    WorkerAnalyticsToLineData,
    WorkerAnalyticsToLineDataAll,
    WorkerAnalyticsToZoneSpecificChartData
} from "src/classLibrary/Charting/reducer/Converter";
import {SecondToHourAggregate, SecondToHourUnit} from "src/classLibrary/Charting/reducer/TimeConverter";
import {SortType} from "@kirinnee/core";


type WorkerGraphType = 'horizontalBar' | 'radar' | 'doughnut' | 'polarArea' | 'pie' | 'line';


const WorkerGraphType: { label: string, value: WorkerGraphType, icon: string }[] = [
    {
        label: 'Bar',
        value: 'horizontalBar',
        icon: 'icofont-chart-bar-graph'
    },
    {
        label: 'Pie',
        value: 'pie',
        icon: 'icofont-chart-pie'
    },
    {
        label: 'Doughnut',
        value: 'doughnut',
        icon: 'icofont-chart-pie-alt'
    },
    {
        label: 'Line',
        value: 'line',
        icon: 'icofont-chart-line'
    },
];

interface WorkerGraphOptionBase {
    projectId: string;
    workerId: string;
    subId: string;
    range: boolean;
    merge: boolean;
    floorId?: string;
    type: WorkerGraphType;
}

interface WorkerGraphOption extends WorkerGraphOptionBase {
    startTime: string;
    endTime: string;
}

export function GenerateTimeArray(startTime: string, endTime: string, days: string[], a: (d: string) => string): string[] {
    const endpoints: string[] = []
    days = days.Map(x => format(new Date(x), 'yyyy/MM/dd'))
    let time = format(new Date(startTime), "yyyy/MM/dd");
    while (new Date(time) <= new Date(endTime)) {
        if (!days.Has(time)) {
            time = format(add(new Date(time), {days: 1}), "yyyy/MM/dd")
            continue
        }
        const n = new Date(time);
        const d = new Date(Date.UTC(n.getUTCFullYear(), n.getMonth(), n.getDate())).toISOString();
        const date = a(d);
        endpoints.push(date);
        time = format(add(new Date(time), {days: 1}), "yyyy/MM/dd")
    }
    return endpoints;
}

function GetStartEnd(days: string[]): [string | null, string | null] {
    const sorted = days.Sort(SortType.Ascending, e => new Date(e) as any as number);
    return [sorted.Take(), sorted.Last()];
}

class WorkerChartRetriever {
    private readonly auth: Auth0;
    private readonly analytics: Api;

    constructor(auth: Auth0, analytics: Api) {
        this.auth = auth;
        this.analytics = analytics;
    }


    async Query(opt: WorkerGraphOption, days: string[], projectName: string): Promise<Result<ChartDataSet | null, ApiError[]>> {
        const token = await this.auth.AnalyticsToken;

        if (opt.type === 'line' && !opt.range) {
            const [start, end] = GetStartEnd(days);
            opt.startTime = start ?? "";
            opt.endTime = end ?? "";
        }


        if (opt.range || opt.type === 'line') {
            const endpoints: string[] = GenerateTimeArray(opt.startTime, opt.endTime, days,
                d => `Analytics/${opt.projectId}/Worker/${opt.workerId}/${d}`);


            const analytics = await Promise.all(
                endpoints.Map(async e => {
                    const r = await this.analytics.Get<WorkerTimeChartResponse>(e, null, token)
                    return {result: r, date: e.split("/").Last()!}
                })
            );
            if (analytics.Any(e => e.result.isErr())) {
                return Err(analytics.Where(e => e.result.isErr())
                                    .Map(e => e.result.unwrapErr()));
            }

            const inter = analytics
                .Where((e: { result: Result<WorkerTimeChartResponse, ApiError>, date: string }) => e.result.isOk())
                .Map(e => {return {timeChart: e.result.unwrap(), date: e.date}});

            const result = inter.Map(x => x.timeChart);
            if (result.length > 0) {
                if (opt.type !== 'line') {
                    return Ok(this.ApplyMerge(ReduceWorkerTimeChart(result), opt.merge, projectName));
                } else {
                    const map = inter.AsValue(v => v.date).MapValue(x => x.timeChart);
                    if (!opt.merge) {
                        const r = WorkerAnalyticsToLineDataAll(map);
                        return Ok(ChartDataSet.Matrix(r));
                    } else {
                        if (opt.floorId == '') opt.floorId = undefined;
                        const r = WorkerAnalyticsToLineData(map, opt.floorId);
                        return Ok(ChartDataSet.Matrix(r));
                    }

                }
            } else {
                return Ok(null);
            }
        } else {
            const endpoint = `Analytics/${opt.projectId}/Worker/${opt.workerId}`;
            const r = await this.analytics.Get<WorkerTimeChartResponse>(endpoint, null, token);
            if (r.isErr()) {
                return Err([r.unwrapErr()])
            }
            const reduced = r.unwrap();
            SecondToHourUnit(reduced.timeSpent);
            SecondToHourAggregate(reduced.floorTimeSpent);
            return Ok(this.ApplyMerge(reduced, opt.merge, projectName));
        }
    }

    ApplyMerge(v: WorkerTimeChartResponse, merge: boolean, projectName: string): ChartDataSet {
        if (!merge) {
            const g = WorkerAnalyticsToZoneSpecificChartData(v)
            return ChartDataSet.Graph(g)
        } else {
            const g = WorkerAnalyticsToChartData(v, projectName);
            return ChartDataSet.Graph(g)
        }
    }

}

export {WorkerChartRetriever, WorkerGraphType, WorkerGraphOption, WorkerGraphOptionBase, GetStartEnd}
