import {Link} from "src/classLibrary/Core/StrongTyping/Link";

const main: Link[] = [
    {
        name: 'Dashboard',
        caption: 'Overview of the project',
        link: '/',
        icon: 'icofont-dashboard'
    },
]

const floor: Link[] = [

    {
        name: 'Floors',
        caption: 'Live-view, floors and floorplans',
        link: '/floor',
        icon: 'icofont-building-alt'
    },
    {
        name: 'Rules',
        caption: 'Configure rules to generate alerts',
        link: '/rule',
        icon: 'icofont-ruler-alt-1'
    },
];
const organization = [
    {
        name: 'Workers',
        caption: 'Manage construction workers',
        link: '/worker',
        icon: 'icofont-worker'
    },
    {
        name: 'SubContractors',
        caption: 'Manage subsidiary contractors',
        link: '/subcontractor',
        icon: 'business'
    },

]

export const developer: Link[] = [
    {
        name: 'Simulator',
        caption: 'Simulate Worker movement',
        link: '/sim',
        icon: 'insights'
    },
    {
        name: 'Overlay Cropper',
        caption: 'Crop Overlays',
        link: '/overlay-cropper',
        icon: 'crop'
    },
]

export const links: Link[][] = [
    main,
    floor,
    organization,
]
