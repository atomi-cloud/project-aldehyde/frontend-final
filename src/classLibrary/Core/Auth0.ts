import {Auth0Client} from "@auth0/auth0-spa-js";
import jwt_decode from 'jwt-decode';

export interface Auth0User {
    email: string;
    email_verified: boolean;
    sub: string;
    picture: string;
    nickname: string;
    name: string;
}

class Auth0 {
    constructor(auth0Client: Auth0Client, redirectCallback: Function) {
        this.auth0Client = auth0Client;
        this.redirectCallback = redirectCallback;
    }

    readonly redirectCallback: Function;
    private _authenticated: boolean = false;
    private loggedInCallback: (() => Promise<void>)[] = [];
    private loggedOutCallback: (() => Promise<void>)[] = [];

    user?: Auth0User | null = null;
    private auth0Client: Auth0Client;
    private coreToken?: string;
    private analyticsToken?: string;
    private lastCall: number = 0;


    async RegisterLogInCallback(callback: () => Promise<void>) {
        this.loggedInCallback.push(callback);
        if (this.isAuthenticated) {
            await callback();
        }
    }

    async RegisterLogOutCallback(callback: () => Promise<void>) {
        this.loggedOutCallback.push(callback);
        if (!this.isAuthenticated) {
            await callback();
        }
    }


    get IsAdmin(): Promise<boolean> {
        return new Promise<boolean>(async (r) => {
            const core = await this.CoreToken;
            const analytics = await this.AnalyticsToken;
            const coreAdmin = (jwt_decode(core) as { [s: string]: string }).scope.split(" ").Has("admin");
            const analyticsAdmin = (jwt_decode(analytics) as { [s: string]: string }).scope.split(" ").Has("admin");
            r(coreAdmin && analyticsAdmin);
        })

    }

    async UpdateUser() {
        await this.auth0Client.getTokenSilently({ignoreCache: true});
        this.user = await this.auth0Client.getUser();
    }

    get isAuthenticated(): boolean {
        return this._authenticated;
    };

    async setAuthenticated(a: boolean) {
        const prev = this._authenticated;
        this._authenticated = a;
        if (prev != a) {
            if (a) {
                this.user = await this.auth0Client.getUser();
                await Promise.all(this.loggedInCallback.Map(e => e()));
            } else {
                this.user = null;
                await Promise.all(this.loggedOutCallback.Map(e => e()));
            }
        }
    }


    async Logout() {
        await this.auth0Client.logout({
            returnTo: window.location.origin
        })
    }

    get AnalyticsToken(): Promise<string> {
        async function lambda(p: Auth0) {
            p.analyticsToken = await p.DeferredToken(p.analyticsToken, process.env.AUTH0_ANALYTICS_AUD);
            return p.analyticsToken!;
        }

        return lambda(this);
    }

    get CoreToken(): Promise<string> {
        async function lambda(p: Auth0) {
            p.coreToken = await p.DeferredToken(p.coreToken, process.env.AUTH0_CORE_AUD);
            return p.coreToken!;
        }

        return lambda(this);
    }

    async DeferredToken(token?: string, audience?: string) {
        if (token) {
            const jwt = jwt_decode(token) as any;
            if (jwt.exp && Date.now() < jwt.exp * 1000) {
                return new Promise<string>((r) => {
                    r(token);
                });
            }
            return this.getNewToken(audience ?? "")
        }
        return this.getNewToken(audience ?? "")
    }

    async getNewToken(audience: string) {
        return await this.auth0Client.getTokenSilently({
            audience,
            scope: "admin"
        });
    }

    async HandleRedirectCallback() {
        try {
            const r = await this.auth0Client.handleRedirectCallback();
            await this.setAuthenticated(true);
            return r;
        } catch (e) {
            console.error(e);
        } finally {
        }
    }

    async Authenticate() {
        if (Date.now() - this.lastCall > 2000) {
            const a = await this.auth0Client.isAuthenticated();
            await this.setAuthenticated(a)

            this.lastCall = Date.now();
        }
    }

    async Login() {
        await this.auth0Client.loginWithRedirect();
    }

}

export {Auth0}
