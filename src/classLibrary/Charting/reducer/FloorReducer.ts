import {
    AnalyticIntermediateUnit,
    DayAnalytics,
    HourAnalytics,
    IsTimeBased
} from "src/classLibrary/APIModels/analytics/CommonAnalyticModels";
import {FloorChartValueType, GraphScope} from "src/classLibrary/Charting/FloorChart";
import {MergeUnit, MergeUnitAverage} from "src/classLibrary/Charting/reducer/IntermediateMerge";
import {ComputeIntermediateAverage, ComputePreIntermediateAverage} from "src/classLibrary/Charting/reducer/Averager";
import {
    SecondToHour,
    SecondToHourAggregate,
    SecondToHourAggregateSegments
} from "src/classLibrary/Charting/reducer/TimeConverter";
import {ScopeHourlyAnalytics} from "src/classLibrary/Charting/reducer/TimeScoper";
import {JoinHourlyAnalytics} from "src/classLibrary/Charting/reducer/Joiner";

function ReduceHourlyAnalytics(a: HourAnalytics[], valueType: FloorChartValueType, graphScope: GraphScope): HourAnalytics {
    // Join
    let merged = a.reduce((a: HourAnalytics | null, b) => JoinHourlyAnalytics(a, b), null)!;

    // To hour
    if (valueType === 'time') {
        SecondToHourAggregateSegments(merged);
    }

    // Scope
    ScopeHourlyAnalytics(merged, graphScope, valueType);
    return merged;
}

function ReduceDayAnalytics(a: DayAnalytics[], valueType: FloorChartValueType): DayAnalytics {
    if (valueType === 'value') {
        const reduced = a.reduce((a: AnalyticIntermediateUnit | null, b) => MergeUnitAverage(a, b), null)!;
        const averaged = ComputePreIntermediateAverage(ComputeIntermediateAverage(reduced));
        return averaged as DayAnalytics;
    } else {
        const reduced = a.reduce((a, b) => MergeUnit(a, b) as DayAnalytics);
        SecondToHour(reduced);
        if (IsTimeBased(reduced)) {
            SecondToHourAggregate(reduced.seconds)
        } else {
            SecondToHourAggregate(reduced.workers)
        }
        return reduced;
    }
}

export {ReduceDayAnalytics, ReduceHourlyAnalytics}
