import {UrlB64ToUint8Array} from "src/classLibrary/Utility";
import {InternalAuth} from "src/classLibrary/Core/InternalAuth";

export async function RegisterServiceWorker(auth: InternalAuth) {
    if ('serviceWorker' in navigator) {
        const registration = await navigator.serviceWorker.register(process.env.SERVICE_WORKER_FILE!)

        const reg = await navigator.serviceWorker.ready;
        const VAPID_KEY = process.env.VAPID_KEY ?? "";

        auth.RegisterLoginCallback(async () => {
            let sub = await reg.pushManager.getSubscription();
            if (sub == null) {
                sub = await registration.pushManager.subscribe({
                    userVisibleOnly: true,
                    applicationServerKey: UrlB64ToUint8Array(VAPID_KEY)
                });
            }
            const o = JSON.parse(JSON.stringify(sub));
            await auth.AddSub({endpoint: o.endpoint, p256hd: o.keys.p256dh, auth: o.keys.auth})
        })


    }
}
