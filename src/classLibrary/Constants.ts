import {FormInput} from "src/classLibrary/FormTypes";
import {ColorMode} from "src/classLibrary/Geometry/ColoredMapper";
import {LabelShape, WorkerColorMode} from "src/classLibrary/Geometry/ShapeDrawer";

export type TitlePosition = 'top' | 'left' | 'bottom' | 'right';


export type GraphExportType = 'jpeg' | 'png' | 'pdf';


export const TitlePositionType: { label: string, value: TitlePosition }[] = [
    {
        label: 'Top',
        value: 'top'
    },
    {
        label: 'Left',
        value: 'left'
    }, {
        label: 'Bottom',
        value: 'bottom'
    }, {
        label: 'Right',
        value: 'right',
    },

]

export const WorkerLabelType: {
    label: string,
    value: LabelShape,
    icon: string
}[] = [
    {
        label: 'Circle',
        value: 'circle',
        icon: 'lens'
    },
    {
        label: 'Pin',
        value: 'droplet',
        icon: 'location_on'
    },
    {
        label: 'Tick',
        value: 'check',
        icon: 'check'
    },
    {
        label: 'Cross',
        value: 'cross',
        icon: 'clear'
    },
    {
        label: 'Arrow',
        value: 'arrow',
        icon: 'south'
    },
    {
        label: 'Triangle',
        value: 'triangle',
        icon: 'icofont-caret-down'
    },
    {
        label: 'Square',
        value: 'square',
        icon: 'icofont-square'
    }
]

export const WorkerColorModes: {
    label: string,
    value: WorkerColorMode,
    description: string,
}[] = [
    {
        label: 'SubContractor',
        value: 'subcon',
        description: `User the worker's subcontractor color`
    },
    {
        label: 'Distinct',
        value: 'distinct',
        description: 'Use a platelet of randomly generated visually distinct colors',
    },
    {
        label: 'Color-Blind',
        value: 'colorblind',
        description: 'Use a platelet of randomly generated visually distinct colors that is color-blind friendly',
    }
];


export const ZoneColorModes: {
    label: string,
    value: ColorMode,
    description: string,
}[] = [
    {
        label: 'Default',
        value: 'original',
        description: 'Use colored defined for each zone when creating floorplan'
    },
    {
        label: 'Distinct',
        value: 'distinct',
        description: 'Use a platelet of randomly generated visually distinct colors',
    },
    {
        label: 'Color-Blind',
        value: 'colorblind',
        description: 'Use a platelet of randomly generated visually distinct colors that is color-blind friendly',
    }
];

export const Actions: { name: string, value: Action, icon: string, disabled?: boolean }[] = [

    {
        name: 'Push Notification',
        value: 'push_notification',
        icon: 'notifications',
    },
    {
        name: 'Send Email',
        value: 'send_email',
        icon: 'email',
        disabled: true,
    },
    {
        name: 'SMS',
        value: 'sms',
        icon: 'sms',
        disabled: true,
    }
]

export type Action = 'send_email' | 'push_notification' | 'sms'
export type AlertLevel = 'warning' | 'success' | 'error' | 'information'
export const AlertLevels: { name: string, value: AlertLevel, icon: string }[] = [
    {
        name: 'Warning',
        value: 'warning',
        icon: 'warning',
    },
    {
        name: 'Success',
        value: 'success',
        icon: 'check',
    },
    {
        name: 'Error',
        value: 'error',
        icon: 'cancel',
    },
    {
        name: 'Information',
        value: 'information',
        icon: 'info',
    },
]

export const ruleModel = {
    labels: [],
    name: '',
    description: '',
}

export const ruleFields: FormInput[] = [
    {
        baseType: 'basic',
        type: 'text',
        max: 128,
        key: 'name',
        label: 'Name',
        icon: 'rule',
        tooltip: 'Name of this rule',
        rules: [
            (e: string) => !!e || 'Message is required',
            (e: string) => e.length > 1 && e.length < 129 || 'Name has to between 2 and 128 characters'
        ]
    },
    {
        baseType: 'basic',
        type: 'text',
        max: 2048,
        key: 'description',
        label: 'Description',
        icon: 'description',
        tooltip: 'Description of this rule',
        rules: [
            (e: string) => !!e || 'Message is required',
            (e: string) => e.length > 1 && e.length < 2049 || 'Name has to between 2 and 2048 characters'
        ]
    },
    {
        baseType: 'select',
        key: 'labels',
        label: 'Labels',
        icon: 'label',
        tooltip: 'Labels of alerts for you to organize your alerts. Alerts created by this rule will be tagged with this alert',
        options: [],
        chip: true,
        multiple: true,
        newValueMode: 'add-unique'
    },

]


export const actionModel = {
    alertLevel: '',
    actionType: '',
    message: '${e.worker.name} has entered danger zone ${e.zone.name}'
}

export const actionFields: FormInput[] = [

    {
        baseType: 'basic',
        type: 'text',
        autogrow: true,
        key: 'message',
        label: 'Message',
        icon: 'message',
        tooltip: 'The message format of the alert',
        rules: [
            (e: string) => !!e || 'Message is required',
        ]
    },
    {
        baseType: 'select',
        key: 'alertLevel',
        label: 'Alert Level',
        icon: 'warning',
        tooltip: 'The priority of the alert. It changes the color and the icon of the message',
        rules: [
            (e: string) => !!e || 'Message is required',
        ],
        options: AlertLevels,
        advance: {labelKey: 'name', valueKey: 'value'},
        iconKey: 'icon'
    },
    {
        baseType: 'select',
        key: 'actionType',
        label: 'Action',
        icon: 'directions_run',
        tooltip: 'Type of action to perform for this alert',
        rules: [
            (e: string) => !!e || 'Message is required',
        ],
        options: Actions,
        disableKey: 'disabled',
        advance: {labelKey: 'name', valueKey: 'value'},
        iconKey: 'icon'
    }
]
