/**
 * Core API v1 (OpenAPI: 3.0.1)
 *
 *
 * NOTE: This class is auto generated by openapi-typescript-client-api-generator.
 * Do not edit the file manually.
 */

export class CreateProjectRequest {
    public name: string;

    public description: string;

    public image: string;

    /**
     * Creates a CreateProjectRequest.
     *
     * @param {string} name
     * @param {string} description
     * @param {string} image
     */
    constructor(name: string, description: string, image: string) {
        this.name = name;
        this.description = description;
        this.image = image;
    }
}
