import {StoreValue} from "src/classLibrary/Core/StoreValue";
import {RAMValue} from "src/classLibrary/Core/RAMValue";
import {FloorPlanSetting} from "src/classLibrary/FloorPlanSettings";
import {GStore} from "@kirinnee/gstore";
import {GraphSetting} from "src/classLibrary/Charting/GraphSetting";


declare module 'vue/types/vue' {
    interface Vue {
        Store: GStore<RAMValue>
        PersistentStore: GStore<StoreValue>;
        FloorPlanSettings: GStore<FloorPlanSetting>;
        GraphSettings: GStore<GraphSetting>;
    }
}

export default {
    install(Vue: any) {
        Vue.mixin({
            mounted() {
                this.Store.StoreValue.loading = false;
            }
        })

        Object.defineProperty(Vue.prototype, "GraphSettings", {
            get(): any {
                return this.$root.graphSetting;
            }
        })
        Object.defineProperty(Vue.prototype, "FloorPlanSettings", {
            get(): any {
                return this.$root.floorPlanSetting;
            }
        })

        Object.defineProperty(Vue.prototype, "PersistentStore", {
            get(): any {
                return this.$root.persistentStore;
            }
        })

        Object.defineProperty(Vue.prototype, "Store", {
            get(): any {
                return this.$root.store;
            }
        })
    }
};


