import {P, Point} from "./Point";
import {SortType} from "@kirinnee/core";
import {CoordinateMapResponse} from "src/classLibrary/APIModels/core/coordinateMapResponse";
import {CoordinateMapRequest} from "src/classLibrary/APIModels/core/coordinateMapRequest";

type Quad = [Point, Point, Point, Point];

type FlattenedQuad = [number, number, number, number, number, number, number, number]

function Q(q: Quad | FlattenedQuad | CoordinateMapResponse | CoordinateMapRequest) {
    return new QuadMap(q);
}

class QuadMap {

    p1: Point;
    p2: Point;
    p3: Point;
    p4: Point;

    Center(): Point {

        const m1 = P(this.p1).MidPoint(P(this.p2));
        const m2 = P(this.p3).MidPoint(P(this.p4));
        return m1.MidPoint(m2).Point;
    }

    ClockWise(): QuadMap {

        const c = P(this.Center());
        const sorted = this.Quad.Sort(SortType.Ascending, x => c.Angle(P(x))) as Quad;
        return new QuadMap(sorted);
    }

    AntiClockWise(): QuadMap {
        const c = P(this.Center());
        const sorted = this.Quad.Sort(SortType.Descending, x => c.Angle(P(x))) as Quad;
        return new QuadMap(sorted);
    }

    Scale(x?: number, y?: number): QuadMap {
        return new QuadMap(
            [
                P(this.p1).Scale(x, y).Point,
                P(this.p2).Scale(x, y).Point,
                P(this.p3).Scale(x, y).Point,
                P(this.p4).Scale(x, y).Point,
            ]
        )
    }

    Percentage(x?: number, y?: number): QuadMap {
        return new QuadMap(
            [
                P(this.p1).Percentage(x, y).Point,
                P(this.p2).Percentage(x, y).Point,
                P(this.p3).Percentage(x, y).Point,
                P(this.p4).Percentage(x, y).Point,
            ]
        )
    }

    Faction(x?: number, y?: number): QuadMap {
        return new QuadMap(
            [
                P(this.p1).Fraction(x, y).Point,
                P(this.p2).Fraction(x, y).Point,
                P(this.p3).Fraction(x, y).Point,
                P(this.p4).Fraction(x, y).Point,
            ]
        )
    }

    get Quad(): Quad {
        return [this.p1, this.p2, this.p3, this.p4];
    }

    get CoordinateMapRequest(): CoordinateMapRequest {
        return new CoordinateMapRequest(
            P(this.p1).PointFRequest,
            P(this.p2).PointFRequest,
            P(this.p3).PointFRequest,
            P(this.p4).PointFRequest
        )
    }

    get FlattenedQuad(): FlattenedQuad {
        return [
            this.p1[0],
            this.p1[1],
            this.p2[0],
            this.p2[1],
            this.p3[0],
            this.p3[1],
            this.p4[0],
            this.p4[1],
        ]
    }

    get CoordinateMapResponse(): CoordinateMapResponse {
        return new CoordinateMapResponse(
            P(this.p1).PointFResponse,
            P(this.p2).PointFResponse,
            P(this.p3).PointFResponse,
            P(this.p4).PointFResponse
        )
    }


    constructor(input: Quad | CoordinateMapResponse | CoordinateMapRequest | FlattenedQuad) {
        if ((<FlattenedQuad>input)[7] !== undefined) {
            const [x1, y1, x2, y2, x3, y3, x4, y4] = <FlattenedQuad>input;

            this.p1 = [x1, y1];
            this.p2 = [x2, y2];
            this.p3 = [x3, y3];
            this.p4 = [x4, y4];
            return;
        }

        if ((<Quad>input)[0] !== undefined) {

            const [a, b, c, d] = <Quad>input;
            this.p1 = a;
            this.p2 = b;
            this.p3 = c;
            this.p4 = d;
            return;
        }


        if ((<CoordinateMapRequest | CoordinateMapResponse>input).item1 !== undefined) {
            const map = <CoordinateMapRequest | CoordinateMapResponse>input;
            this.p1 = [map.item1.x, map.item1.y];
            this.p2 = [map.item2.x, map.item2.y];
            this.p3 = [map.item3.x, map.item3.y];
            this.p4 = [map.item4.x, map.item4.y];
            return;
        }
        throw Error("unknown quad type")
    }
}


export {Quad, Q, QuadMap, FlattenedQuad};
