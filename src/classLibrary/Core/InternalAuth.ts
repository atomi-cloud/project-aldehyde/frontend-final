import {UserResponse} from "src/classLibrary/APIModels/core/userResponse";
import {Api, ApiError} from "src/classLibrary/Core/Api";
import {Err, Ok, Result} from "@hqoss/monads";
import {Auth0} from "src/classLibrary/Core/Auth0";
import Vue from 'vue';
import {UserPrincipalResponse} from "src/classLibrary/APIModels/core/userPrincipalResponse";
import {StoreValue} from "src/classLibrary/Core/StoreValue";
import {FloorPlanSetting} from "src/classLibrary/FloorPlanSettings";
import {GStore} from "@kirinnee/gstore";
import {GraphSetting} from "src/classLibrary/Charting/GraphSetting";


class InternalAuth {
    private coreApi: Api;
    private auth0: Auth0;

    constructor(coreApi: Api, auth0: Auth0, settings: { store: GStore<StoreValue>, floorplan: GStore<FloorPlanSetting>, graph: GStore<GraphSetting> }) {
        this.coreApi = coreApi;
        this.auth0 = auth0;
        this.settings = settings;

    }

    user?: UserResponse | null = null;

    private settings: { store: GStore<StoreValue>, floorplan: GStore<FloorPlanSetting>, graph: GStore<GraphSetting> }

    private loginCallback: ((() => void)) [] = [];
    private logoutCallback: ((() => void))[] = [];

    RegisterLogoutCallback(callback: (() => void)) {
        this.logoutCallback.push(callback);
        if (this.user == null)
            callback();

    }

    RegisterLoginCallback(callback: (() => void)) {
        this.loginCallback.push(callback);
        if (this.user != null)
            callback();

    }

    async AddSub(subscription: { endpoint: string, p256hd: string, auth: string }): Promise<Result<UserPrincipalResponse, ApiError>> {
        const token = await this.auth0.CoreToken;
        return await this.coreApi.Post<UserPrincipalResponse>(`User/subscription/${this.user?.principal.id}`, {
            endpoint: subscription.endpoint,
            p256hd: subscription.p256hd,
            auth: subscription.auth,
        }, token);
    }

    ChangeKey() {
        if (this.user == null) {
            this.settings.store.changeKey("app-settings");
            this.settings.floorplan.changeKey("floorplan-settings");
            this.settings.graph.changeKey("graph-setting");
        } else {
            const prefix = 'builderlytics-user-';
            this.settings.store.changeKey(prefix + this.user?.principal.username);
            this.settings.floorplan.changeKey(prefix + this.user?.principal.username + "-fp");
            this.settings.graph.changeKey(prefix + this.user?.principal.username + "-graph");
        }
    }

    async Registered(): Promise<Result<boolean, ApiError>> {
        const token = await this.auth0.CoreToken;
        const r = await this.coreApi.Get<UserResponse>(`User/sub/${this.auth0.user?.sub}`, null, token)
        const p = this;
        return r
            .map((u) => {
                Vue.set(parent, 'user', u)
                p.user = u;
                p.ChangeKey();
                p.loginCallback.Each(e => e());
                return true;
            })
            .match({
                ok: val => Ok(val),
                err: (v: ApiError): Result<boolean, ApiError> => ApiError.if.Null<Result<boolean, ApiError>>(v,
                    () => {
                        p.user = null;
                        p.logoutCallback.Each(e => e());

                        return Ok(false)
                    },
                    (_v) => Err(_v),
                )
            });
    }

}

export {InternalAuth}
