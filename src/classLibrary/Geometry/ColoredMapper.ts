import {InputZone} from "src/classLibrary/Geometry/ZoneMapper";
import {InputExit} from "src/classLibrary/Geometry/ExitMapper";
import {ColoredFloorPlan, ColoredLine, ColoredQuad} from "src/classLibrary/Geometry/Colored";
import {Q} from "src/classLibrary/Geometry/Quad";
import {P} from "src/classLibrary/Geometry/Point";
import iwanthue from 'iwanthue';

export type ColorMode = 'original' | 'distinct' | 'colorblind';

class ColoredMapper {

    private readonly mode: ColorMode;


    To(width: number, height: number,
       z: Map<string, InputZone>, e: Map<string, InputExit>): ColoredFloorPlan {

        const zones = new Map<string, InputZone>(JSON.parse(JSON.stringify(z.Arr())));
        const exits = new Map<string, InputExit>(JSON.parse(JSON.stringify(e.Arr())));

        let colorSpace: Map<string, string>;
        const length = zones.size;
        if (this.mode === 'original') {
            colorSpace = zones.Keys().AsKey((v) => zones.get(v)!.color);
        } else if (this.mode === 'distinct') {
            const cs = [0, 360, 15, 40, 70, 100] as any;
            const options = {
                colorSpace: cs,
            } as any;
            const colors = iwanthue(length, options);
            colorSpace = zones.Keys().AsKey((_, i) => colors[i])
        } else {
            const cs = [0, 360, 40, 70, 15, 85] as any;
            const options = {
                colorSpace: cs,
            } as any;
            const colors = iwanthue(length, options);
            colorSpace = zones.Keys().AsKey((_, i) => colors[i])

        }

        const quads = zones
            .MapValue((v, k) => {
                v.color = colorSpace.get(k)!;
                return v;
            })
            .Map((k, v) => this.toZones(k, v, width, height))
            .Flatten<ColoredQuad>()
            .Map(x => x.Data);
        const lines = exits
            .Map((k, v) => this.toExits(k, v, zones, width, height))
            .Map(x => x.Data);

        return new ColoredFloorPlan({
                width,
                height,
                quads,
                lines
            }
        )
    }

    private toZones(name: string, z: InputZone, w: number, h: number): ColoredQuad[] {
        return z.maps.Map(q => new ColoredQuad({
            quad: Q(q).Faction(w, h).Quad,
            color: z.color,
            name,
            danger: z.danger
        }))
    }

    private toExits(name: string, e: InputExit, zones: Map<string, InputZone>, w: number, h: number): ColoredLine {

        const [p1, p2] = e.p;
        const a1 = zones.get(e.a1)!;
        const a2 = zones.get(e.a2)!;
        return new ColoredLine({
                p1: P(p1).Fraction(w, h).Point,
                p2: P(p2).Fraction(w, h).Point,
                color1: a1.color,
                color2: a2.color,
                name
            }
        )
    }

    constructor(mode: ColorMode) {
        this.mode = mode;
    }
}

export {ColoredMapper}
