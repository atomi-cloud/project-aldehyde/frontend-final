import {
    AggregateProjectionData,
    AggregateProjectionDataIntermediate,
    IZoneDatabase,
    IZoneFloorDatabase,
    IZoneSubDatabase,
    ProjectionData,
    ProjectionDataIntermediate
} from "src/classLibrary/APIModels/analytics/CommonAnalyticModels";

function MergeZone(prev: IZoneDatabase, next: IZoneDatabase) {
    for (let k in next.zones) {
        if (next.zones.hasOwnProperty(k)) {
            if (prev.zones[k] == null) {
                prev.zones[k] = next.zones[k]
            }
        }
    }
}

function MergeFloorAndZone(prev: IZoneFloorDatabase, next: IZoneFloorDatabase) {
    // Merge SubContractor
    for (let k in next.floors) {
        if (next.floors.hasOwnProperty(k)) {
            if (prev.floors[k] == null) {
                prev.floors[k] = next.floors[k]
            }
        }
    }
    // Merge zones
    MergeZone(prev, next);
}

function MergeZoneAndSub(prev: IZoneSubDatabase, next: IZoneSubDatabase) {
    // Merge SubContractor
    for (let k in next.subcontractors) {
        if (next.subcontractors.hasOwnProperty(k)) {
            if (prev.subcontractors[k] == null) {
                prev.subcontractors[k] = next.subcontractors[k]
            }
        }
    }
    // Merge zones
    MergeZone(prev, next);
}

// Aggregate
function MergeAggregateProjectDataAverage(prev: AggregateProjectionDataIntermediate, next: AggregateProjectionData) {
    for (let key in next) {
        if (next.hasOwnProperty(key)) {
            if (prev[key] == null) prev[key] = [0, 0];
            prev[key][0] += next[key];
            prev[key][1]++;
        }
    }
    return prev;
}

function MergeAggregateProjectData(prev: AggregateProjectionData, next: AggregateProjectionData) {
    for (let key in next) {
        if (next.hasOwnProperty(key)) {
            if (prev[key] == null) prev[key] = 0;
            prev[key] += next[key];
        }
    }
}

function MergeProjectDataAverage(prev: ProjectionDataIntermediate, next: ProjectionData) {
    for (let outerKey in next) {
        if (next.hasOwnProperty(outerKey)) {
            const nextData = next[outerKey];
            for (let innerKey in nextData) {
                if (nextData.hasOwnProperty(innerKey)) {
                    if (prev[outerKey] == null) prev[outerKey] = {}
                    if (prev[outerKey][innerKey] == null) prev[outerKey][innerKey] = [0, 0];
                    prev[outerKey][innerKey][0] += next[outerKey][innerKey];
                    prev[outerKey][innerKey][1]++;
                }
            }
        }
    }
}

function MergeProjectData(prev: ProjectionData, next: ProjectionData) {
    for (let outerKey in next) {
        if (next.hasOwnProperty(outerKey)) {
            const nextData = next[outerKey];
            for (let innerKey in nextData) {
                if (nextData.hasOwnProperty(innerKey)) {
                    if (prev[outerKey] == null) prev[outerKey] = {}
                    if (prev[outerKey][innerKey] == null) prev[outerKey][innerKey] = 0;
                    prev[outerKey][innerKey] += next[outerKey][innerKey];
                }
            }
        }
    }
}

export {
    MergeZoneAndSub,
    MergeAggregateProjectDataAverage,
    MergeAggregateProjectData,
    MergeProjectDataAverage,
    MergeProjectData,
    MergeFloorAndZone,
    MergeZone,
}
