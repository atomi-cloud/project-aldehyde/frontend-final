export interface StandardDotNetError {
    type: string;
    title: string;
    status: number;
    tradeId: string;
    errors: { [s: string]: string[] }
}
