import {Graph, GraphValue} from "src/classLibrary/Charting/DataSet";
import {ChartData, ChartScales, ChartTooltipItem, ChartTooltipOptions, CommonAxe} from "chart.js";
import {FormatDuration} from "src/classLibrary/Utility";
import {GraphSetting} from "src/classLibrary/Charting/GraphSetting";
import tinycolor from "tinycolor2";
import {GraphScope} from "src/classLibrary/Charting/FloorChart";
import {format} from "date-fns";

function GenerateLabelSettings(settings: GraphSetting, graph: Graph): any {
    const {r, g, b} = tinycolor(settings.labelBackgroundColor).toRgb();
    const backgroundColor = `rgba(${r},${g}, ${b}, ${settings.labelBackgroundTransparency}`;
    return {

        formatter: LabelFormatting(graph.valueType, settings.shortenDuration),
        color: settings.labelColor,
        backgroundColor,
        font: {
            size: settings.labelSize,
        },
        display: function (context: any) {
            let index = context.dataIndex;
            let value = context.dataset.data[index];

            const isZero = value === 0;

            return settings.labelGraph && (!isZero || settings.zeroLabel);
        }
    }
}

function LabelFormatting(valueType: GraphValue, short: boolean): ((value: number | string) => string) {
    return function (value: number | string): string {
        return GraphValue.match<string>(valueType, {
            Date(s: GraphScope) {
                return GraphScope.match<string>(s, {
                    Day() {
                        return format(new Date(value), "dd MMM yy")
                    }, Hour() {
                        return format(new Date(value), "dd MMM HHaa")
                    }, Month() {
                        return format(new Date(value), "MMM yy")
                    }
                });
            }, Float() {
                return value.ToFloat().toString();
            }, Hours() {
                const seconds = value.ToFloat() * 3600;
                return FormatDuration({seconds}, short);
            }, Int() {
                return value.ToInt().toString();
            }, Precision(p0: number) {
                return value.ToFloat().toFixed(p0);
            }, SignificantFigure(p0: number) {
                return value.ToFloat().toPrecision(p0);
            }, Original() {
                return value.toString();
            }
        });
    }
}

function GenerateAxes(stacked: boolean, type: GraphValue, shortenDuration: boolean, id: string | null = null): CommonAxe {
    return {
        id: id ? id : undefined,
        stacked,
        gridLines: {
            drawTicks: true,
            drawOnChartArea: true,
        },
        position: !id ? 'left' : 'right',
        ticks: {
            callback(value: number | string): string | number | null | undefined {
                const formatter = LabelFormatting(type, shortenDuration);
                return formatter(value);
            },
            autoSkip: true,
            autoSkipPadding: 40,
            maxRotation: 0,
        },
        scaleLabel: {
            display: false
        },
    }
}

function GenerateToolTips(graph: Graph): ChartTooltipOptions {
    const formatLabels = LabelFormatting(graph.independentType, false);
    return {
        mode: 'index',
        intersect: false,
        callbacks: {
            label: function (tooltipItem: ChartTooltipItem, data: ChartData): string | string[] {
                const dataObject = data.datasets![tooltipItem.datasetIndex!];
                let label = dataObject.label ?? "";
                if (!label.IsEmpty()) {
                    // label = formatLabels(label);
                    label += ": ";
                }
                const value = dataObject.data![tooltipItem.index!];
                const formatter = LabelFormatting(graph.valueType, false);
                const formatted = formatter(value as number | string);
                return label + formatted;
            },
            title(item: ChartTooltipItem[], chart: ChartData): string | string[] {
                if (graph.type === 'radar' || graph.type === 'pie') {
                    return item.Map(e => chart.labels![e.index!] as string).Unique();
                }
                return item.Map(e => formatLabels(e.label as string)).Unique();
            }
        }
    }
}


function GenerateScales(stacked: boolean, shortenDuration: boolean, graph: Graph): ChartScales | undefined {
    // @ts-ignore
    switch (graph.type) {
        case "line":
        case "bar":
        case "scatter":
        case "bubble":
            const r = {
                xAxes: [
                    GenerateAxes(stacked, graph.independentType, shortenDuration)
                ],
                yAxes: [
                    GenerateAxes(stacked, graph.dependentType, shortenDuration),

                ]
            }
            if (graph.type == 'bar') {
                r.yAxes.push(GenerateAxes(stacked, graph.dependentType, shortenDuration, "cumulative-axis"))
            }
            return r;
        case "horizontalBar":
            return {
                yAxes: [
                    GenerateAxes(stacked, graph.independentType, shortenDuration)
                ],
                xAxes: [
                    GenerateAxes(stacked, graph.dependentType, shortenDuration)
                ]
            };
        case "radar":
        case "polarArea":
        case "pie":
        case "doughnut":
            return {
                xAxes: [],
                yAxes: [],
            };
    }
}


export {GenerateScales, GenerateToolTips, LabelFormatting, GenerateLabelSettings}
