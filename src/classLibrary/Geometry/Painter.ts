import {QuadMap} from "src/classLibrary/Geometry/Quad";
import {PointMap} from "src/classLibrary/Geometry/Point";

import {ColoredFloorPlan, ColoredLine, ColoredQuad} from "src/classLibrary/Geometry/Colored";

import tinycolor from "tinycolor2";

interface PainterConfig {
    width: number
    height: number;
    zoneAlpha: number;
    exitAlpha: number;
    exitWidth: number;
    showDanger: boolean;
    dangerIntensity: number;
    drawZone: boolean;
    drawExit: boolean;
}

class Painter {

    private readonly ctx: CanvasRenderingContext2D;
    private readonly config: PainterConfig = {
        width: 1,
        height: 1,
        zoneAlpha: 0.3,
        exitAlpha: 1,
        exitWidth: 1,
        showDanger: true,
        dangerIntensity: 10,
        drawZone: true,
        drawExit: true,
    }


    private diagonalCanvas(): HTMLCanvasElement {

        const pattern = document.createElement("CANVAS") as HTMLCanvasElement;
        const w = this.config.dangerIntensity;
        const h = this.config.dangerIntensity;
        pattern.width = w;
        pattern.height = h;
        const ctx = pattern.getContext("2d")!;
        ctx.strokeStyle = "#FF0000";
        ctx.moveTo(0, w);
        ctx.lineTo(h, 0);
        ctx.stroke();
        return pattern;
    }

    private get width() {
        return this.config.width;
    }

    private get height() {
        return this.config.height;
    }


    Configure(config: Partial<PainterConfig>) {
        Object.assign(this.config, config);
    }

    private clear() {
        this.ctx.clearRect(0, 0, this.config.width, this.config.height);
    }

    private traceQuad(area: QuadMap) {
        this.ctx.beginPath();
        const [p1, p2, p3, p4] = area.Quad;
        this.move(p1);
        this.lineTo(p2);
        this.lineTo(p3);
        this.lineTo(p4);
        this.ctx.closePath();
    }

    private traceLine(p1: PointMap, p2: PointMap) {
        this.ctx.beginPath();
        this.move(p1.Point);
        this.lineTo(p2.Point);
        this.ctx.closePath();
    }

    private move(p: [number, number]) {
        const [x, y] = p;
        this.ctx.moveTo(x, y);
    }

    private lineTo(p: [number, number]) {
        const [x, y] = p;
        this.ctx.lineTo(x, y);
    }

    private fill(style: string | CanvasGradient | CanvasPattern) {
        this.ctx.fillStyle = style;
        this.ctx.fill();
    }

    private stroke(style: string | CanvasGradient | CanvasPattern, width: number) {
        this.ctx.strokeStyle = style;
        this.ctx.lineWidth = width;
        this.ctx.stroke();
    }

    private drawQuad(q: ColoredQuad) {
        const {r, g, b} = tinycolor(q.color).toRgb();


        const fixed = q.quad.Scale(this.width, this.height).ClockWise();

        if (this.config.drawZone) {
            this.traceQuad(fixed);
            this.fill(`rgba(${r},${g},${b},${this.config.zoneAlpha})`);
        }
        if (q.danger && this.config.showDanger) {
            const pattern = this.ctx.createPattern(this.diagonalCanvas(), "repeat")!;
            this.traceQuad(fixed);
            this.fill(pattern);
        }
    }

    private drawExit(e: ColoredLine) {
        if (this.config.drawExit) {
            const p1 = e.p1.Scale(this.width, this.height);
            const p2 = e.p2.Scale(this.width, this.height);
            const mid = p1.MidPoint(p2);
            {
                const {r, g, b} = tinycolor(e.color1).toRgb();
                const color1 = `rgba(${r}, ${g}, ${b}, ${this.config.exitAlpha})`;
                this.traceLine(p1, mid);
                this.stroke(color1, this.config.width)
            }
            {
                const {r, g, b} = tinycolor(e.color2).toRgb();
                const color2 = `rgba(${r}, ${g}, ${b}, ${this.config.exitAlpha})`;
                this.traceLine(mid, p2);
                this.stroke(color2, this.config.width);
            }

        }

    }

    Draw(fp: ColoredFloorPlan) {
        this.clear();
        this.ctx.canvas.width = this.width;
        this.ctx.canvas.height = this.height;

        fp.lines.Each((e) => {

            this.drawExit(e)
        })
        fp.quads.Each((q) => {

            this.drawQuad(q)
        })
    };

    constructor(config: PainterConfig, ctx: CanvasRenderingContext2D) {
        this.config = config;
        this.ctx = ctx;
    }


}

export {Painter, PainterConfig}
