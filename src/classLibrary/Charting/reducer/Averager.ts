import {
    AnalyticIntermediateUnit,
    AnalyticPreIntermediateUnit,
    AnalyticUnit,
    IsCountBased,
    IsTimeBased
} from "src/classLibrary/APIModels/analytics/CommonAnalyticModels";


function ComputePreIntermediateAverage(next: AnalyticPreIntermediateUnit): AnalyticUnit {
    const ret = next as any as AnalyticUnit;
    for (let outerKey in next.data) {
        if (next.data.hasOwnProperty(outerKey)) {
            const nextData = next.data[outerKey];
            for (let innerKey in nextData) {
                if (nextData.hasOwnProperty(innerKey)) {
                    const [value, amount] = next.data[outerKey][innerKey];
                    ret.data[outerKey][innerKey] = value / amount;
                }
            }
        }
    }
    return ret;
}

function ComputeIntermediateAverage(next: AnalyticIntermediateUnit): AnalyticPreIntermediateUnit {
    const ret = next as any as AnalyticPreIntermediateUnit;
    if (IsCountBased(next) && IsCountBased(ret)) {
        for (let key in next.workers) {
            if (next.workers.hasOwnProperty(key)) {
                const [value, amount] = next.workers[key];
                ret.workers[key] = value / amount;
            }
        }
        return ret;
    } else if (IsTimeBased(next) && IsTimeBased(ret)) {
        for (let key in next.seconds) {
            if (next.seconds.hasOwnProperty(key)) {
                const [value, amount] = next.seconds[key];
                ret.seconds[key] = value / amount;
            }
        }
        return ret;
    } else {
        throw new Error("Neither count nor time based analytics (does not align pre-post casting")
    }

}

function ConvertToIntermediate(next: AnalyticPreIntermediateUnit): AnalyticIntermediateUnit {
    const ret = next as any as AnalyticIntermediateUnit;
    if (IsCountBased(next) && IsCountBased(ret)) {
        for (let key in next.workers) {
            if (next.workers.hasOwnProperty(key)) {
                ret.workers[key] = [next.workers[key], 1];
            }
        }
        return ret;
    } else if (IsTimeBased(next) && IsTimeBased(ret)) {
        for (let key in next.seconds) {
            if (next.seconds.hasOwnProperty(key)) {
                ret.seconds[key] = [next.seconds[key], 1];
            }
        }
        return ret;
    } else {
        throw new Error("Neither count nor time based analytics (does not align pre-post casting")
    }
}

function ConvertToPreIntermediate(next: AnalyticUnit): AnalyticPreIntermediateUnit {
    const ret = next as any as AnalyticPreIntermediateUnit;
    for (let outerKey in next.data) {
        if (next.data.hasOwnProperty(outerKey)) {
            const nextData = next.data[outerKey];
            for (let innerKey in nextData) {
                if (nextData.hasOwnProperty(innerKey)) {
                    ret.data[outerKey][innerKey] = [next.data[outerKey][innerKey], 1];
                }
            }
        }
    }
    return ret;
}


export {ComputePreIntermediateAverage, ConvertToPreIntermediate, ComputeIntermediateAverage, ConvertToIntermediate}
