import {
    AnalyticIntermediateUnit,
    AnalyticUnit,
    IsCountBased,
    IsTimeBased
} from "src/classLibrary/APIModels/analytics/CommonAnalyticModels";
import {
    MergeAggregateProjectData,
    MergeAggregateProjectDataAverage,
    MergeProjectData,
    MergeProjectDataAverage,
    MergeZoneAndSub
} from "src/classLibrary/Charting/reducer/BasicMerge";
import {ConvertToIntermediate, ConvertToPreIntermediate} from "src/classLibrary/Charting/reducer/Averager";

function MergeUnit(prev: AnalyticUnit, next: AnalyticUnit): AnalyticUnit {
    MergeZoneAndSub(prev, next);
    MergeProjectData(prev.data, next.data);
    if (IsTimeBased(prev) && IsTimeBased(next)) {
        MergeAggregateProjectData(prev.seconds, next.seconds);
    } else if (IsCountBased(prev) && IsCountBased(next)) {
        MergeAggregateProjectData(prev.workers, next.workers);
    }
    return prev;
}

function MergeUnitAverage(prev: AnalyticIntermediateUnit | null, next: AnalyticUnit): AnalyticIntermediateUnit {
    if (prev == null) {
        const pre = ConvertToPreIntermediate(next);
        return ConvertToIntermediate(pre);
    } else {
        MergeZoneAndSub(prev, next);
        MergeProjectDataAverage(prev.data, next.data);
        if (IsTimeBased(prev) && IsTimeBased(next)) {
            MergeAggregateProjectDataAverage(prev.seconds, next.seconds);
        } else if (IsCountBased(prev) && IsCountBased(next)) {
            MergeAggregateProjectDataAverage(prev.workers, next.workers);
        }
        return prev;
    }

}

export {MergeUnit, MergeUnitAverage}
