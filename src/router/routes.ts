import {RouteConfig} from 'vue-router';
import Error404 from "pages/Error404.vue";
import Index from "pages/Index.vue";
import Home from "pages/Home.vue";
import Register from "pages/Register.vue";
import {floorRoutes} from "src/router/floor.routes";
import {subcontractorRoutes} from "src/router/subcontractor.routes";
import {workerRoutes} from "src/router/worker.routes";
import {ruleRoutes} from "src/router/rule.routes";
import {projectRoutes} from "src/router/project.routes";
import {adminRoutes} from "src/router/admin.routes";
import {widgetRoutes} from "src/router/widget.routes";

const routes: RouteConfig[] = [
    {
        name: 'dashboard',
        path: '/',
        component: Index,
        meta: {
            transition: {},
            auth: [false, true]
        }
    },
    {
        name: 'home',
        path: '/home',
        component: Home,
        meta: {
            auth: [true, true]
        }
    },
    {
        name: 'register',
        path: '/register',
        component: Register,
        meta: {
            limbo: true,
            auth: [false, false]
        }
    },
    ...projectRoutes,
    ...floorRoutes,
    ...subcontractorRoutes,
    ...workerRoutes,
    ...ruleRoutes,
    ...adminRoutes,
    ...widgetRoutes,
    // Always leave this as last one,
    // but you can also remove it
    {
        path: '*',
        component: Error404
    }
];

export default routes;
