import SubContractors from "pages/SubContractors.vue";
import CreateSubContractor from "components/Forms/CreateSubContractor.vue";
import {Route, RouteConfig} from 'vue-router';
import EditSubContractor from "components/Forms/EditSubContractor.vue";
import {Tab} from "src/classLibrary/Core/StrongTyping/Tabs";
import SubContractorAlerts from "pages/SubContractor/SubContractorAlerts.vue";
import SubContractorOverview from "pages/SubContractor/SubContractorOverview.vue";
import SubContractorWorkers from "pages/SubContractor/SubContractorWorkers.vue";

function SubContractorTabs(route: Route): Tab[] {
    return [
        {
            label: 'OverView',
            link: `/subcontractor/${route.params.subId}`,
            icon: 'track_changes'
        },
        {
            label: 'Workers',
            link: `/subcontractor/${route.params.subId}/workers`,
            icon: 'icofont-worker',

        },
        {
            label: 'Alerts',
            link: `/subcontractor/${route.params.subId}/alert`,
            icon: 'icofont-notification'
        },
    ]
}

export const subcontractorRoutes: RouteConfig[] = [
    {
        name: 'subcontractors',
        path: '/subcontractor',
        component: SubContractors,
        meta: {
            limbo: false,
            auth: [false, true]
        }
    },
    {
        name: 'create-subcontractor',
        path: '/subcontractor/create',
        component: CreateSubContractor,
        meta: {
            limbo: false,
            auth: [false, true]
        }
    },
    {
        name: 'subcontractor-edit',
        path: '/subcontractor/edit/:subId',
        component: EditSubContractor,
        meta: {
            limbo: false,
            auth: [false, true]
        }
    },
    {

        name: 'subcontractor-workers',
        path: '/subcontractor/:subId/workers',
        component: SubContractorWorkers,
        meta: {
            limbo: false,
            auth: [false, true],
            tabs: SubContractorTabs
        }
    },
    {

        name: 'subcontractor-alerts',
        path: '/subcontractor/:subId/alert',
        component: SubContractorAlerts,
        meta: {
            limbo: false,
            auth: [false, true],
            tabs: SubContractorTabs
        }
    },
    {

        name: 'subcontractor',
        path: '/subcontractor/:subId/',
        component: SubContractorOverview,
        meta: {
            limbo: false,
            auth: [false, true],
            tabs: SubContractorTabs
        }
    },
]
