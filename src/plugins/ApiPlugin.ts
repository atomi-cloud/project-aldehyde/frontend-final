import {Api} from "src/classLibrary/Core/Api";

declare module 'vue/types/vue' {
    interface Vue {
        $coreApi: Api;
        $analyticsApi: Api;
    }
}

export default {
    install(Vue: any, options: { core: Api, analytics: Api }) {
        Vue.prototype.$coreApi = options.core;
        Vue.prototype.$analyticsApi = options.analytics;
    }
};


