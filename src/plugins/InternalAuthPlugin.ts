import {InternalAuth} from "src/classLibrary/Core/InternalAuth";
import {UserResponse} from "src/classLibrary/APIModels/core/userResponse";

declare module 'vue/types/vue' {
    interface Vue {
        $coreAuth: InternalAuth,
        InternalUser?: UserResponse
    }
}
export default {
    install(Vue: any, options: InternalAuth) {

        const o = Vue.observable(options);
        Vue.prototype.$coreAuth = o;

        Vue.mixin({
            data: function () {
                return {
                    InternalUser: null
                }
            },
            watch: {
                '$coreAuth.user': {
                    deep: true,
                    immediate: true,
                    handler: function () {
                        Vue.set(this, "InternalUser", o.user)
                    },
                }
            }
        })
    },
};
