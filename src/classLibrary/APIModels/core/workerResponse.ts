/**
 * Core API v1 (OpenAPI: 3.0.1)
 *
 *
 * NOTE: This class is auto generated by openapi-typescript-client-api-generator.
 * Do not edit the file manually.
 */

import {WorkerPrincipalResponse} from "./workerPrincipalResponse";
import {SubContractorPrincipalResponse} from "./subContractorPrincipalResponse";
import {TagPrincipalResponse} from "./tagPrincipalResponse";

export class WorkerResponse {
    public principal: WorkerPrincipalResponse;

    public subContractor: SubContractorPrincipalResponse;

    public tag: TagPrincipalResponse;

    /**
     * Creates a WorkerResponse.
     *
     * @param {WorkerPrincipalResponse} principal
     * @param {SubContractorPrincipalResponse} subContractor
     * @param {TagPrincipalResponse} tag
     */
    constructor(principal: WorkerPrincipalResponse, subContractor: SubContractorPrincipalResponse, tag: TagPrincipalResponse) {
        this.principal = principal;
        this.subContractor = subContractor;
        this.tag = tag;
    }
}
