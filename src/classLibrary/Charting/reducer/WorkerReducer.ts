import {WorkerTimeChartResponse} from "src/classLibrary/APIModels/analytics/workerTimeChartResponse";
import {
    MergeAggregateProjectData,
    MergeFloorAndZone,
    MergeProjectData
} from "src/classLibrary/Charting/reducer/BasicMerge";
import {SecondToHourAggregate, SecondToHourUnit} from "src/classLibrary/Charting/reducer/TimeConverter";

function ReduceWorkerTimeChart(worker: WorkerTimeChartResponse[]): WorkerTimeChartResponse {
    const w = worker.reduce((a: WorkerTimeChartResponse | null, b) => MergeWorkerTimeChart(a, b), null)!;
    SecondToHourUnit(w.timeSpent);
    SecondToHourAggregate(w.floorTimeSpent);
    return w;
}

function MergeWorkerTimeChart(prev: WorkerTimeChartResponse | null, next: WorkerTimeChartResponse): WorkerTimeChartResponse {
    if (prev == null) {
        return next;
    } else {
        MergeProjectData(prev.timeSpent, next.timeSpent);
        MergeAggregateProjectData(prev.floorTimeSpent, next.floorTimeSpent);
        MergeFloorAndZone(prev, next);
        return prev;
    }
}

export {ReduceWorkerTimeChart, MergeWorkerTimeChart}
