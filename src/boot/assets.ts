import Vue from "vue";
import icecream from "../assets/animations/icecream.json";
import noTissue from "../assets/animations/tissue.json";
import chemicalExplosion from "../assets/animations/chemical.json";
import alienInvasion from "../assets/animations/cow.json";
import catGlass from "../assets/animations/cat.json";
import dogFindingLostObject from "../assets/animations/dogSmell.json";
import coffeeSplit from "../assets/animations/coffee.json";
import lochness from "../assets/animations/lochness.json";
import dogEatsNewspaper from "../assets/animations/dogNewsPaper.json";
import lostInSpace from "../assets/animations/astronaout.json";
import laptopBroken from "../assets/animations/laptop.json";
import doggyCantSwim from "../assets/animations/dogSwimming.json";
import missingPiece from "../assets/animations/puzzle.json";
import emptyBox from "../assets/animations/emptybox.json";
import logo from "../assets/builderlytics.png";

const animations = {
    emptyBox,
    errors: {
        lochness,
        icecream,
        noTissue,
        chemicalExplosion,
        alienInvasion,
        catGlass,
        dogFindingLostObject,
        missingPiece,
        coffeeSplit,
        dogEatsNewspaper,
        lostInSpace,
        laptopBroken,
        doggyCantSwim
    }
}

export const goodOnes = [1, 3, 4, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 21, 23, 24, 25, 28, 29, 31, 32, 34, 36, 38];

const md: { [s: string]: string } = {};
for (let i = 0; i < goodOnes.length; i++) {
    md[`${goodOnes[i]}index`] = require(`../assets/md-background/${goodOnes[i]}.jpg`);
}

const images = {
    logo,
    md,
}

declare module 'vue/types/vue' {

    interface Vue {
        $animations: {
            emptyBox: object,
            errors: {
                lochness: object,
                icecream: object,
                noTissue: object,
                chemicalExplosion: object,
                alienInvasion: object,
                catGlass: object,
                dogFindingLostObject: object,
                missingPiece: object,
                coffeeSplit: object,
                dogEatsNewspaper: object,
                lostInSpace: object,
                laptopBroken: object,
                doggyCantSwim: object,

            }
        }

        $images: { logo: string, md: { [s: string]: string } }
    }
}

Vue.prototype.$animations = animations;
Vue.prototype.$images = images;

