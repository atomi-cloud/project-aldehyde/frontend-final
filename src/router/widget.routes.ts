import {RouteConfig} from "vue-router";
import WidgetType from "pages/Widget/WidgetType.vue";
import ChartWidgetType from "pages/Widget/ChartWidgetType.vue";
import TextWidgetType from "pages/Widget/TextWidgetType.vue";
import WorkerChartType from "pages/Widget/WorkerChartType.vue";
import FloorChartType from "pages/Widget/FloorChartType.vue";
import SingleTextType from "pages/Widget/SingleTextType.vue";
import CompareTextType from "pages/Widget/CompareTextType.vue";
import RelativeTextType from "pages/Widget/RelativeTextType.vue";

export const widgetRoutes: RouteConfig[] = [


    {
        name: 'choose-widget-chart-worker',
        path: '/widget/type/chart/worker',
        component: WorkerChartType,
        meta: {
            limbo: false,
            auth: [false, true],
        }
    },
    {
        name: 'choose-widget-chart-floor',
        path: '/widget/type/chart/floor',
        component: FloorChartType,
        meta: {
            limbo: false,
            auth: [false, true],
        }
    },
    {
        name: 'choose-widget-chart',
        path: '/widget/type/chart',
        component: ChartWidgetType,
        meta: {
            limbo: false,
            auth: [false, true],
        }
    },
    {
        name: 'choose-widget-text-relative',
        path: '/widget/type/text/relative',
        component: RelativeTextType,
        meta: {
            limbo: false,
            auth: [false, true],
        }
    },
    {
        name: 'choose-widget-text-compare',
        path: '/widget/type/text/compare',
        component: CompareTextType,
        meta: {
            limbo: false,
            auth: [false, true],
        }
    },
    {
        name: 'choose-widget-text-single',
        path: '/widget/type/text/single',
        component: SingleTextType,
        meta: {
            limbo: false,
            auth: [false, true],
        }
    },
    {
        name: 'choose-widget-text',
        path: '/widget/type/text',
        component: TextWidgetType,
        meta: {
            limbo: false,
            auth: [false, true],
        }
    },
    {
        name: 'choose-widget-type',
        path: '/widget/type',
        component: WidgetType,
        meta: {
            limbo: false,
            auth: [false, true],
        }
    },
];
