export type WorkerColorMode = 'subcon' | 'distinct' | 'colorblind';
export type WorkerLabelMode = 'none' | 'name' | 'name-short' | 'id' | 'id-short';
export type LabelShape = 'circle' | 'square' | 'triangle' | 'cross' | 'check' | 'droplet' | 'arrow';

export interface ShapeOption {
    shape: LabelShape;
    color: string;
    dottedBorder: boolean;
    fill: boolean;
    size: number;
    glow: boolean
}

export class ShapeDrawer {
    private readonly ctx: CanvasRenderingContext2D;
    private readonly shapeOption: ShapeOption = {
        dottedBorder: false,
        color: 'blue',
        fill: true,
        size: 5,
        shape: 'circle',
        glow: false,
    }

    private clear() {
        this.ctx.clearRect(0, 0, this.shapeOption.size, this.shapeOption.size);
    }

    Configure(config: Partial<ShapeOption>) {
        Object.assign(this.shapeOption, config);
    }

    private move(p: [number, number]) {
        const [x, y] = p;
        this.ctx.moveTo(x, y);
    }

    private lineTo(p: [number, number]) {
        const [x, y] = p;
        this.ctx.lineTo(x, y);
    }


    private stroke() {
        const size = this.shapeOption.size * 100;
        if (this.shapeOption.dottedBorder) {
            const dash = size / 8;
            const space = size / 16;
            this.ctx.setLineDash([dash, space]);
        } else {
            this.ctx.setLineDash([]);
        }
        this.ctx.lineWidth = 200;
        this.ctx.strokeStyle = this.shapeOption.color;
        this.ctx.stroke();
    }

    private fill() {
        if (this.shapeOption.fill) {
            this.ctx.fillStyle = this.shapeOption.color;
            this.ctx.fill();
        }
    }

    private DrawCircle() {
        const size = this.shapeOption.size * 100;
        this.ctx.beginPath();
        this.ctx.arc(size / 2, size / 2, size / 2 - 1200, 0, 2 * Math.PI)
        this.ctx.closePath();
        this.stroke();
        this.fill();
    }

    private DrawSquare() {
        const size = this.shapeOption.size * 100;
        this.ctx.beginPath();
        this.move([1200, 1200]);
        this.lineTo([1200, size - 1200])
        this.lineTo([size - 1200, size - 1200])
        this.lineTo([size - 1200, 1200])
        this.ctx.closePath();
        this.stroke();
        this.fill();
    }

    private DrawTriangle() {
        const size = this.shapeOption.size * 100;
        this.ctx.beginPath();
        this.move([1200, 1200]);
        this.lineTo([size - 1200, 1200])
        this.lineTo([size / 2, size - 1200])
        this.ctx.closePath();
        this.stroke();
        this.fill();
    }

    private DrawCross() {
        const s = this.shapeOption.size * 100;

        const thick = s / 6;
        const center = s / 2;

        this.ctx.beginPath();
        this.ctx.moveTo(center, center - thick);
        this.ctx.lineTo(s - thick, 0);
        this.ctx.lineTo(s, thick);
        this.ctx.lineTo(center + thick, center);
        this.ctx.lineTo(s, s - thick);
        this.ctx.lineTo(s - thick, s);
        this.ctx.lineTo(center, center + thick);
        this.ctx.lineTo(thick, s);
        this.ctx.lineTo(0, s - thick);
        this.ctx.lineTo(center - thick, center);
        this.ctx.lineTo(0, thick);
        this.ctx.lineTo(thick, 0);
        this.ctx.closePath();
        this.stroke();
        this.fill();
    }

    Draw() {
        this.clear();
        this.ctx.canvas.width = this.shapeOption.size * 100;
        this.ctx.canvas.height = this.shapeOption.size * 100;
        if (this.shapeOption.glow) {
            this.ctx.shadowBlur = 1200;
            this.ctx.shadowColor = this.shapeOption.color;
        }

        switch (this.shapeOption.shape) {
            case "circle":
                return this.DrawCircle();
            case "square":
                return this.DrawSquare();
            case "triangle":
                return this.DrawTriangle();
            case "cross":
                return this.DrawCross();

        }
    }

    constructor(ctx: CanvasRenderingContext2D) {
        this.ctx = ctx;
    }

}
