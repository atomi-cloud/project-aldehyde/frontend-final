import {RelativeTimeConfig} from "src/classLibrary/Dashboard/Dashboard";
import {of, Union} from "ts-union";
import {Api, ApiError} from "src/classLibrary/Core/Api";
import {Auth0} from "src/classLibrary/Core/Auth0";
import {ToStartEnd} from "src/classLibrary/Dashboard/DashboardUtility";
import {WorkerTimeChartResponse} from "src/classLibrary/APIModels/analytics/workerTimeChartResponse";
import {Err, Ok, Result} from "@hqoss/monads";
import {GenerateTimeArray, GetStartEnd} from "src/classLibrary/Charting/WorkerChart";
import {MergeWorkerTimeChart} from "src/classLibrary/Charting/reducer/WorkerReducer";
import {differenceInCalendarDays} from "date-fns";
import {
    AnalyticIntermediateUnit,
    DayAnalytics,
    IsTimeBased
} from "src/classLibrary/APIModels/analytics/CommonAnalyticModels";
import {MergeUnit, MergeUnitAverage} from "src/classLibrary/Charting/reducer/IntermediateMerge";
import {ComputeIntermediateAverage, ComputePreIntermediateAverage} from "src/classLibrary/Charting/reducer/Averager";

const TimeQuery = Union({
    All: of<void>(),
    Range: of<RelativeTimeConfig>(),
})

type TimeQuery = typeof TimeQuery.T;

const DurationMetric = Union({
    TotalDuration: of<void>(),
    AverageDuration: of<void>(),
})

type DurationMetric = typeof DurationMetric.T;


const AllMetric = Union({
    Count: of<void>(),
    Duration: of<DurationMetric>(),
});

type AllMetric = typeof AllMetric.T;

interface FloorSingleMetricQueryOptions {
    floorId: string;
    time: TimeQuery;
    metric: AllMetric;
    trades?: string[]
    subId?: string;
}

interface WorkerSingleMetricQueryOptions {
    workerId: string;
    time: TimeQuery;
    metric: DurationMetric;

    floorId?: string;
}

// interface SubContractorSingleMetricQueryOptions {
//     subcontractorId: string;
//     time: TimeQuery;
//     metric: AllMetric,
//     // Filter
//     floorId?: string;
// }

const SingleMetric = Union({
    // SubContractor: of<SubContractorSingleMetricQueryOptions>(),
    Worker: of<WorkerSingleMetricQueryOptions>(),
    Floor: of<FloorSingleMetricQueryOptions>(),
})

type SingleMetric = typeof SingleMetric.T;

const MetricValue = Union({
    Duration: of<number>(),
    Count: of<number>(),
})

type MetricValue = typeof MetricValue.T

class SingleMetricQuerier {

    private readonly analytics: Api;
    private readonly auth: Auth0;
    private readonly project: string;

    // async GetAllFloors(): Promise<string[]> {
    //
    // }

    async GetWorker(workerId: string, time: TimeQuery): Promise<Result<WorkerTimeChartResponse | null, ApiError>> {
        const p = this;
        const token = await p.auth.AnalyticsToken;
        return await TimeQuery.match<Promise<Result<WorkerTimeChartResponse | null, ApiError>>>(time, {
            async All() {
                return await p.analytics.Get<WorkerTimeChartResponse>(
                    `Analytics/${p.project}/Worker/${workerId}`, null, token);

            },
            async Range(c: RelativeTimeConfig): Promise<Result<WorkerTimeChartResponse | null, ApiError>> {
                const [start, end] = ToStartEnd(c, new Date());
                const dayR = await p.analytics.Get<string[]>(`Analytics/${p.project}/Worker/${workerId}/days`, null, token);
                if (dayR.isErr()) {
                    return Err(dayR.unwrapErr())
                }
                const days = dayR.unwrap();
                const endpoints: string[] = GenerateTimeArray(start, end, days,
                    d => `Analytics/${p.project}/Worker/${workerId}/${d}`);
                const analytics = await Promise.all(
                    endpoints.Map(e => p.analytics.Get<WorkerTimeChartResponse>(e, null, token))
                );
                if (analytics.Any(e => e.isErr())) {
                    return Err(analytics.Where(e => e.isErr())
                                        .Map(e => e.unwrapErr()).Take()!);
                }
                const results = analytics.Where(x => x.isOk())
                                         .Map(x => x.unwrap());
                if (results.length > 0) {
                    const reduced = results.reduce((a: WorkerTimeChartResponse | null, b) => MergeWorkerTimeChart(a, b), null)!
                    return Ok(reduced);
                } else {
                    return Ok(null);
                }

            }

        })
    }

    async GetWorkerDateDiff(time: TimeQuery, workerId: string): Promise<Result<number, ApiError>> {
        const token = await this.auth.AnalyticsToken;
        const p = this;
        return await TimeQuery.match<Promise<Result<number, ApiError>>>(time, {
            async All() {
                const dayR = await p.analytics.Get<string[]>(`Analytics/${p.project}/Worker/${workerId}/days`, null, token);
                if (dayR.isErr()) {
                    return Err(dayR.unwrapErr())
                }
                const days = dayR.unwrap();
                const [start, end] = GetStartEnd(days);
                if (start && end) {
                    const s = new Date(start), e = new Date(end);
                    const diff = differenceInCalendarDays(s, e).Abs();
                    return Ok(diff);
                } else {
                    return Ok(0);
                }
            },
            Range(c: RelativeTimeConfig) {
                const [start, end] = ToStartEnd(c, new Date());
                return new Promise<Result<number, ApiError>>(r => {
                    const s = new Date(start), e = new Date(end);
                    const diff = differenceInCalendarDays(s, e).Abs();
                    r(Ok(diff))
                });

            }
        })
    }

    async WorkerQuery(worker: WorkerSingleMetricQueryOptions): Promise<Result<number, ApiError>> {
        const r = await this.GetWorker(worker.workerId, worker.time);
        if (r.isErr()) {
            return Err(r.unwrapErr());
        }
        const chart = r.unwrap();
        if (chart == null) return Ok(0);


        let time = 0;
        if (worker.floorId) {
            if (chart.floorTimeSpent[worker.floorId]) {
                time = chart.floorTimeSpent[worker.floorId];
            }
        } else {
            for (let k in chart.floorTimeSpent) {
                if (chart.floorTimeSpent.hasOwnProperty(k)) {
                    time += chart.floorTimeSpent[k];
                }
            }
        }

        const p = this;
        return await DurationMetric.match<Promise<Result<number, ApiError>>>(worker.metric, {
            async AverageDuration() {
                const r = await p.GetWorkerDateDiff(worker.time, worker.workerId);
                return r.map(e => time / e);
            },
            TotalDuration() {
                return new Promise<Result<number, ApiError>>(r => {
                    r(Ok(time));
                });
            }

        })
    }


    FloorTimeQuery(days: string[], time: TimeQuery): [string | null, string | null] {
        return TimeQuery.match(time, {
            All() {
                return GetStartEnd(days);
            }, Range(c: RelativeTimeConfig) {
                return ToStartEnd(c, new Date());

            }

        })
    }

    ReduceDayAnalytics(result: DayAnalytics[], valueType: 'count' | 'seconds'): DayAnalytics {
        if (valueType === 'count') {
            const reduced = result.reduce((a: AnalyticIntermediateUnit | null, b) => MergeUnitAverage(a, b), null)!;
            const averaged = ComputePreIntermediateAverage(ComputeIntermediateAverage(reduced));
            return averaged as DayAnalytics;
        } else {
            return result.reduce((a, b) => MergeUnit(a, b) as DayAnalytics);

        }
    }

    // Get All floors
    async FloorQuery(floor: FloorSingleMetricQueryOptions): Promise<Result<number, ApiError>> {
        const token = await this.auth.AnalyticsToken;
        const dayR = await this.analytics.Get<string[]>(`Analytics/${this.project}/${floor.floorId}/days`, null, token);
        if (dayR.isErr()) return Err(dayR.unwrapErr());
        const days = dayR.unwrap();

        const [start, end] = this.FloorTimeQuery(days, floor.time);
        if (!start || !end) return Ok(0);

        const diff = differenceInCalendarDays(new Date(start), new Date(end)).Abs();

        const metric = AllMetric.match<'count' | 'seconds'>(floor.metric, {
            Count() {
                return 'count';
            }, Duration() {
                return 'seconds';
            }
        })
        const query = floor.trades?.Map(x => `trades=${x}`)?.join("&");

        const endpoints: string[] = GenerateTimeArray(start, end, days,
            d => `Analytics/${this.project}/${floor.floorId}/${d}/day/subcontractor/zone/${metric}?${query}`);
        const analytics = await Promise.all(endpoints.Map(e => this.analytics.Get<DayAnalytics>(e, null, token)));
        if (analytics.Any(e => e.isErr())) {
            return Err(analytics.Where(e => e.isErr())
                                .Map(e => e.unwrapErr()).Take()!);
        }
        const result = analytics
            .Where((e: Result<DayAnalytics, ApiError>) => e.isOk())
            .Map(e => e.unwrap());
        if (result.length > 0) {
            let total = 0;
            const reduced = this.ReduceDayAnalytics(result, metric);
            if (IsTimeBased(reduced)) {
                for (let subId in reduced.seconds) {
                    if (reduced.seconds.hasOwnProperty(subId)) {
                        if (floor.subId) {
                            if (subId === floor.subId) {
                                total = reduced.seconds[subId];
                            }
                        } else {
                            total += reduced.seconds[subId]
                        }
                    }
                }
                const dura = AllMetric.if.Duration(floor.metric, e => e)!;
                return Ok(DurationMetric.match(dura, {
                    AverageDuration() {
                        return total / diff;
                    }, TotalDuration() {
                        return total;
                    }
                }));
            } else {
                for (let subId in reduced.workers) {
                    if (reduced.workers.hasOwnProperty(subId)) {
                        if (floor.subId) {
                            if (subId === floor.subId) {
                                total = reduced.workers[subId];
                            }
                        } else {
                            total += reduced.workers[subId]
                        }
                    }
                }
                return Ok(total)
            }
        } else {
            return Ok(0);
        }

    }

    async Query(metric: SingleMetric): Promise<Result<MetricValue, ApiError>> {
        const p = this;
        return await SingleMetric.match<Promise<Result<MetricValue, ApiError>>>(metric, {
            async Floor(fm: FloorSingleMetricQueryOptions) {
                const r = await p.FloorQuery(fm);
                if (r.isErr()) return Err(r.unwrapErr());
                const f = r.unwrap();
                return Ok(AllMetric.match(fm.metric, {
                    Count() {
                        return MetricValue.Count(f);
                    }, Duration() {
                        return MetricValue.Duration(f)
                    }
                }));
            },
            async Worker(wm: WorkerSingleMetricQueryOptions) {
                const r = await p.WorkerQuery(wm);
                return r.map(seconds => MetricValue.Duration(seconds));
            }

        });
    }

    constructor(analytics: Api, auth: Auth0, project: string) {
        this.analytics = analytics;
        this.auth = auth;
        this.project = project;
    }
}

export {
    SingleMetricQuerier,
    SingleMetric,
    FloorSingleMetricQueryOptions,
    WorkerSingleMetricQueryOptions,
    AllMetric,
    DurationMetric,
    TimeQuery,
    MetricValue
}


