import {Auth0} from "src/classLibrary/Core/Auth0";

declare module 'vue/types/vue' {
    interface Vue {
        $auth: Auth0,
        LoggedIn: boolean,
    }
}
export default {
    install(Vue: any, options: Auth0) {

        const o = Vue.observable(options);
        Vue.prototype.$auth = o;

        Vue.mixin({
            data: function () {
                return {
                    LoggedIn: false
                }
            },
            watch: {
                '$auth.isAuthenticated': {
                    deep: true,
                    immediate: true,
                    handler: function () {
                        Vue.set(this, "LoggedIn", o.isAuthenticated)
                    },
                }
            }
        })
    },
};
