/**
 * Core API v1 (OpenAPI: 3.0.1)
 *
 *
 * NOTE: This class is auto generated by openapi-typescript-client-api-generator.
 * Do not edit the file manually.
 */

import {ZonePublicRequest} from "./zonePublicRequest";
import {ExitPublicRequest} from "./exitPublicRequest";

export class FloorPlanPublicRequest {
    public id: string;

    public name: string;

    public timeStamp: string;

    public zones: ZonePublicRequest[];

    public exits: ExitPublicRequest[];

    /**
     * Creates a FloorPlanPublicRequest.
     *
     * @param {string} id
     * @param {string} name
     * @param {string} timeStamp
     * @param {ZonePublicRequest} zones
     * @param {ExitPublicRequest} exits
     */
    constructor(id: string, name: string, timeStamp: string, zones: ZonePublicRequest[], exits: ExitPublicRequest[]) {
        this.id = id;
        this.name = name;
        this.timeStamp = timeStamp;
        this.zones = zones;
        this.exits = exits;
    }
}
