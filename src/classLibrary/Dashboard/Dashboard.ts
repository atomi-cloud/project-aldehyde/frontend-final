import {of, Union} from "ts-union";
import {WorkerChartWidget} from "src/classLibrary/Dashboard/WorkerChartWidget";
import {FloorChartWidget} from "src/classLibrary/Dashboard/FloorChartWidget";
import {SingleValueTextWidget} from "src/classLibrary/Dashboard/SingleValueTextWidget";
import {CompareTextWidget} from "src/classLibrary/Dashboard/CompareWidget";
import {RelativeTextWidget} from "src/classLibrary/Dashboard/RelativeTextWidget";

interface RelativeTimeConfig {
    start: number;
    amount: number;
}

const ChartWidget = Union({
    Worker: of<WorkerChartWidget>(),
    Floor: of<FloorChartWidget>(),
})

type ChartWidget = typeof ChartWidget.T;

const TextWidget = Union({
    Single: of<SingleValueTextWidget>(),
    Compare: of<CompareTextWidget>(),
    Relative: of<RelativeTextWidget>(),
})

type TextWidget = typeof TextWidget.T;

const WidgetData = Union({
    Chart: of<ChartWidget>(),
    Text: of<TextWidget>(),
});

type WidgetData = typeof WidgetData.T;

interface Widget {
    id: string;
    name: string;
    widget: WidgetData;
}


export {RelativeTimeConfig, ChartWidget, Widget, WidgetData, TextWidget}
