import {ZoneLightResponse} from "src/classLibrary/APIModels/analytics/zoneLightResponse";
import {SubContractorPrincipalResponse} from "src/classLibrary/APIModels/analytics/subContractorPrincipalResponse";
import {DayZoneSubcontractorSecondResponse} from "src/classLibrary/APIModels/analytics/dayZoneSubcontractorSecondResponse";
import {DayZoneSubcontractorWorkerResponse} from "src/classLibrary/APIModels/analytics/dayZoneSubcontractorWorkerResponse";
import {DaySubcontractorZoneSecondResponse} from "src/classLibrary/APIModels/analytics/daySubcontractorZoneSecondResponse";
import {DaySubcontractorZoneWorkerResponse} from "src/classLibrary/APIModels/analytics/daySubcontractorZoneWorkerResponse";
import {HourlySubcontractorZoneSecondResponse} from "src/classLibrary/APIModels/analytics/hourlySubcontractorZoneSecondResponse";
import {HourlySubcontractorZoneWorkerResponse} from "src/classLibrary/APIModels/analytics/hourlySubcontractorZoneWorkerResponse";
import {HourlyZoneSubcontractorSecondResponse} from "src/classLibrary/APIModels/analytics/hourlyZoneSubcontractorSecondResponse";
import {HourlyZoneSubcontractorWorkerResponse} from "src/classLibrary/APIModels/analytics/hourlyZoneSubcontractorWorkerResponse";
import {SubcontractorZoneSecondResponse} from "src/classLibrary/APIModels/analytics/subcontractorZoneSecondResponse";
import {SubcontractorZoneWorkerResponse} from "src/classLibrary/APIModels/analytics/subcontractorZoneWorkerResponse";
import {ZoneSubcontractorWorkerResponse} from "src/classLibrary/APIModels/analytics/zoneSubcontractorWorkerResponse";
import {ZoneSubcontractorSecondResponse} from "src/classLibrary/APIModels/analytics/zoneSubcontractorSecondResponse";
import {FloorPrincipalResponse} from "src/classLibrary/APIModels/analytics/floorPrincipalResponse";


/*
 INFORMATION OF DATA MERGING, JOINING, SCOPING AND REDUCTION

 1. TYPE VS INTERFACE
 TYPE (PROJECTION DATA/AGGREGATE PROJECT DATA) ARE DICTIONARY ALIAS
 INTERFACE (I PROJECTION DATA, I SECOND ANALYTICS ETC) ARE INTERFACES THAT CONTAINS THE TYPE AS A MEMBER

 2. INTERMEDIATE TYPE VS NON-INTERMEDIATE TYPE
 INTERMEDIATE TYPES ARE TUPLE OF [VALUE, COUNT] TO COMPUTE AVERAGE
 => NON-INTERMEDIATE ARRAY  (MERGE=>) INTERMEDIATE ARRAY (AVERAGE=>) NON INTERMEDIATE ARRAY

 3. PRE-INTERMEDIATE VS INTERMEDIATE INTERFACES
 PRE INTERMEDIATE:
 DATA (PROJECTION DATA) HAS BEEN CONVERTED INTO PROJECTION DATA INTERMEDIATE
 SECONDS/WORKER (AGGREGATE DATA) HAS NOT BEEN CONVERTED
 INTERMEDIATE:
 DATA (PROJECTION DATA) HAS BEEN CONVERTED INTO PROJECTION DATA INTERMEDIATE
 SECONDS/WORKER (AGGREGATE DATA) HAS BEEN CONVERTED INTO PROJECT DATA INTERMEDIATE

 */

// Building blocks
type ZoneDict = { [s: string]: ZoneLightResponse };
type SubDict = { [s: string]: SubContractorPrincipalResponse };
type FloorDict = { [s: string]: FloorPrincipalResponse };

type AggregateProjectionData = { [s: string]: number }
type AggregateProjectionDataIntermediate = { [s: string]: [number, number] }

type ProjectionDataIntermediate = { [s: string]: { [s: string]: [number, number] } };
type ProjectionData = { [s: string]: { [s: string]: number } };

// Building Block interfaces
interface IZoneDatabase {
    zones: ZoneDict;
}

interface IFloorDatabase {
    floors: FloorDict;
}

interface ISubDatabase {
    subcontractors: SubDict;
}

interface IZoneSubDatabase extends IZoneDatabase, ISubDatabase {
}

interface IZoneFloorDatabase extends IFloorDatabase, IZoneDatabase {
}


interface IProjectDataIntermediate extends IZoneSubDatabase {
    data: ProjectionDataIntermediate
}

interface IProjectionData extends IZoneSubDatabase {
    data: ProjectionData;
}

// Unit Pre Intermediate
interface ICountAnalyticsPreIntermediate extends IProjectDataIntermediate {
    workers: AggregateProjectionData;
}

interface ISecondsAnalyticsPreIntermediate extends IProjectDataIntermediate {
    seconds: AggregateProjectionData;
}

interface SegmentAggregateCountAnalyticsPreIntermediate {
    data: { [s: string]: ICountAnalyticsPreIntermediate }
}

interface SegmentAggregateSecondAnalyticsPreIntermediate {
    data: { [s: string]: ISecondsAnalyticsPreIntermediate }
}

type  SegmentAggregateAnalyticsPreIntermediate =
    SegmentAggregateCountAnalyticsPreIntermediate
    | SegmentAggregateSecondAnalyticsPreIntermediate;

// Unit Intermediate
interface ICountAnalyticsIntermediate extends IProjectDataIntermediate {
    workers: AggregateProjectionDataIntermediate;
}

interface ISecondsAnalyticsIntermediate extends IProjectDataIntermediate {
    seconds: AggregateProjectionDataIntermediate;
}

interface SegmentAggregateCountAnalyticsIntermediate {
    data: { [s: string]: ICountAnalyticsIntermediate }
}

interface SegmentAggregateSecondAnalyticsIntermediate {
    data: { [s: string]: ISecondsAnalyticsIntermediate }
}

type  SegmentAggregateAnalyticsIntermediate =
    SegmentAggregateCountAnalyticsIntermediate
    | SegmentAggregateSecondAnalyticsIntermediate;

// Unit
interface ICountAnalytics extends IProjectionData {
    workers: AggregateProjectionData;
}

interface ISecondsAnalytics extends IProjectionData {
    seconds: AggregateProjectionData;
}

interface SegmentAggregateCountAnalytics {
    data: { [s: string]: ICountAnalytics }
}

interface SegmentAggregateSecondAnalytics {
    data: { [s: string]: ISecondsAnalytics }
}

type  SegmentAggregateAnalytics = SegmentAggregateCountAnalytics | SegmentAggregateSecondAnalytics;


type DayAnalytics = DayZoneSubcontractorSecondResponse
    | DayZoneSubcontractorWorkerResponse
    | DaySubcontractorZoneSecondResponse
    | DaySubcontractorZoneWorkerResponse

type HourAnalytics = HourlySubcontractorZoneSecondResponse
    | HourlySubcontractorZoneWorkerResponse
    | HourlyZoneSubcontractorSecondResponse
    | HourlyZoneSubcontractorWorkerResponse

type AnalyticUnit = ICountAnalytics | ISecondsAnalytics;

type AnalyticPreIntermediateUnit = ICountAnalyticsPreIntermediate | ISecondsAnalyticsPreIntermediate
type AnalyticIntermediateUnit = ICountAnalyticsIntermediate | ISecondsAnalyticsIntermediate

type CountBaseAnalytic = ICountAnalytics | ICountAnalyticsIntermediate | ICountAnalyticsPreIntermediate;
type TimeBaseAnalytic = ISecondsAnalytics | ISecondsAnalyticsIntermediate | ISecondsAnalyticsPreIntermediate;
type AnalyticCommon = AnalyticUnit | AnalyticPreIntermediateUnit | AnalyticIntermediateUnit;

type SegmentAnalytics = SubcontractorZoneSecondResponse
    | SubcontractorZoneWorkerResponse
    | ZoneSubcontractorWorkerResponse
    | ZoneSubcontractorSecondResponse


function IsTimeBased(a: AnalyticCommon): a is TimeBaseAnalytic {
    return (a as TimeBaseAnalytic).seconds !== undefined
}

function IsCountBased(a: AnalyticCommon): a is CountBaseAnalytic {
    return (a as CountBaseAnalytic).workers !== undefined
}

export {
    IsTimeBased,
    IsCountBased,
    IFloorDatabase,
    IZoneDatabase,
    ISubDatabase,
    IZoneFloorDatabase,
    FloorDict,
    ProjectionDataIntermediate,
    CountBaseAnalytic,
    TimeBaseAnalytic,
    DayAnalytics,
    HourAnalytics,
    AnalyticUnit,
    AnalyticPreIntermediateUnit,
    AnalyticIntermediateUnit,
    SegmentAnalytics,
    IProjectionData,
    ProjectionData,
    ZoneDict,
    SubDict,
    AggregateProjectionData,
    IZoneSubDatabase,
    AggregateProjectionDataIntermediate,
    IProjectDataIntermediate,
    ICountAnalytics,
    ISecondsAnalytics,
    ICountAnalyticsIntermediate,
    ISecondsAnalyticsIntermediate,
    SegmentAggregateCountAnalytics,
    SegmentAggregateAnalytics,
    SegmentAggregateSecondAnalytics,
    ICountAnalyticsPreIntermediate,
    ISecondsAnalyticsPreIntermediate,
    SegmentAggregateAnalyticsIntermediate,
    SegmentAggregateAnalyticsPreIntermediate,
    SegmentAggregateCountAnalyticsIntermediate,
    SegmentAggregateCountAnalyticsPreIntermediate,
    SegmentAggregateSecondAnalyticsIntermediate,
    SegmentAggregateSecondAnalyticsPreIntermediate,
}
