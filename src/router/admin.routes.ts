import {RouteConfig} from 'vue-router';
import SimulatorList from "pages/Developers/SimulatorList.vue";
import Simulator from "pages/Developers/Simulator.vue";
import OverlayCropper from "pages/Developers/OverlayCropper.vue";

export const adminRoutes: RouteConfig[] = [
    {
        name: 'overlay-cropper',
        path: '/overlay-cropper',
        component: OverlayCropper,
        meta: {
            limbo: false,
            auth: [false, true]
        }
    },
    {
        name: 'simulator-list',
        path: '/sim',
        component: SimulatorList,
        meta: {
            limbo: false,
            auth: [false, true]
        }
    },
    {
        name: 'simulator',
        path: '/sim/:floorId',
        component: Simulator,
        meta: {
            limbo: false,
            auth: [false, true]
        }
    },

]
