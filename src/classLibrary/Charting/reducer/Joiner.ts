import {HourAnalytics} from "src/classLibrary/APIModels/analytics/CommonAnalyticModels";
import {MergeZoneAndSub} from "src/classLibrary/Charting/reducer/BasicMerge";

function ConvertKeys(hour: HourAnalytics): HourAnalytics {

    const d = new Date(hour.info.day);
    for (let k in hour.data) {
        if (hour.data.hasOwnProperty(k)) {
            const h = k.toString().ToInt()

            let day = new Date(d.getFullYear(), d.getMonth(), d.getDate(), h);
            hour.data["or:" + day.toISOString()] = hour.data[k];
            delete hour.data[k];
        }
    }
    return hour;

}

function JoinHourlyAnalytics(prev: HourAnalytics | null, next: HourAnalytics): HourAnalytics {
    if (prev == null) {
        return ConvertKeys(next);
    } else {
        next = ConvertKeys(next);
        MergeZoneAndSub(prev, next);
        for (let k in next.data) {
            if (next.data.hasOwnProperty(k)) {
                prev.data[k] = next.data[k];
            }
        }
        return prev;
    }
}

export {JoinHourlyAnalytics}
